1. Update
  Port : #define UDPPROC_SERVICE_PORT 47685 (UDP)
  
  Structure :
	typedef struct _udpmsg {
		int command;
		int socketNumber;
		int type;
		char ip_addr[20];
		char com_name[50];
	} UDPMSG;
	
  Message (Put UDPMSG - 'int command') :
    enum command {
		UDP_EXTERNAL_MSG_UPDATE_START = 100,
		UDP_EXTERNAL_MSG_UPDATE_END = 101
	}