#pragma once
#include<stdio.h>
#include<string.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#define UDPPROC_SERVICE_PORT 47685
#define UDPPROC_CONSOLE_PORT 47686
#define UDPPROC_GUI_PORT 47687
#define UDPPROC_KVM0_PORT 47688
#define UDPPROC_KVM1_PORT 47689

#define BUFSIZE 1024

typedef struct _udpmsg {
	int command;
	int socketNumber;
	int type;
	char ip_addr[20];
	char com_name[50];
} UDPMSG;

enum command {
	UDP_ALLOW_REMOTE = 0,
	UDP_DISALLOW_REMOTE,
	UDP_REQUEST_REMOTE_LIST,
	UDP_REQUEST_REMOTE_LIST_ACK,
	UDP_REQUEST_FORCE_DISCONNECT,
	UDP_GUI_TERMINATE,
	UDP_GUI_REQUEST_REDDOT,
	UDP_GUI_REDDOT_ON,
	UDP_GUI_REDDOT_OFF,
	UDP_GUI_CURSOR_CHANGED,
	UDP_KVM_TERMINATE,
//	UDP_KVM_CURSOR_CHANGED,
	UDP_TEMP,
	UDP_EXTERNAL_MSG_UPDATE_START = 100,
	UDP_EXTERNAL_MSG_UPDATE_END = 101
} m_udpCommand;

//// Console
int m_udpSocket;
struct sockaddr_in m_udpServ, m_udpClient;
socklen_t m_udpSockLen;

void UdpInit();
void UdpConnect(int port);
void UdpSend(UDPMSG udpmsg);

// Not used
void UdpSendTo(char *msg);
void UdpBind(int port);
int UdpRecvFrom(char *buffer);


//// GUI
int m_udpSocketGui;
struct sockaddr_in m_udpServGui, m_udpClientGui;
socklen_t m_udpSockLenGui;

void UdpInitGui();
void UdpConnectGui(int port);
void UdpSendGui(UDPMSG udpmsg);


//// KVM
int m_udpSocketKVM0;
struct sockaddr_in m_udpServKVM0, m_udpClientKVM0;
socklen_t m_udpSockLenKVM0;
int m_udpSocketKVM1;
struct sockaddr_in m_udpServKVM1, m_udpClientKVM1;
socklen_t m_udpSockLenKVM1;

void UdpInitKVM();
void UdpConnectKVM();
void UdpSendKVM(UDPMSG udpmsg);
