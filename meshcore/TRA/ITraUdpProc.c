#include "ITraUdpProc.h"

void UdpInit()
{
	m_udpSocket = socket(AF_INET, SOCK_DGRAM, 0);
}

void UdpConnect(int port)
{
	m_udpServ.sin_family = AF_INET;
	m_udpServ.sin_port = htons(port);
	m_udpServ.sin_addr.s_addr = inet_addr("127.0.0.1");
	m_udpSockLen = sizeof(m_udpServ);
}

void UdpSend(UDPMSG udpmsg)
{
	sendto(m_udpSocket, (char*)&udpmsg, sizeof(udpmsg), 0, (struct sockaddr *)&m_udpServ, m_udpSockLen);
}

void UdpSendTo(char *buffer)
{
	sendto(m_udpSocket, buffer, sizeof(buffer), 0, (struct sockaddr *)&m_udpServ, m_udpSockLen);
}

void UdpBind(int port)
{
	m_udpServ.sin_family = AF_INET;
	m_udpServ.sin_port = htons(port);
	m_udpServ.sin_addr.s_addr = htonl(INADDR_ANY);
	m_udpSockLen = sizeof(m_udpServ);

	memset((char*)&m_udpClient, 0x00, m_udpSockLen);

	bind(m_udpSocket, (struct sockaddr*)&m_udpServ, m_udpSockLen);
}

int UdpRecvFrom(char *buffer)
{
	return recvfrom(m_udpSocket, buffer, 512, 0, (struct sockaddr*)&m_udpClient, &m_udpSockLen);
}


void UdpInitGui()
{
	m_udpSocketGui = socket(AF_INET, SOCK_DGRAM, 0);
}

void UdpConnectGui(int port)
{
	m_udpServGui.sin_family = AF_INET;
	m_udpServGui.sin_port = htons(port);
	m_udpServGui.sin_addr.s_addr = inet_addr("127.0.0.1");
	m_udpSockLenGui = sizeof(m_udpServGui);
}

void UdpSendGui(UDPMSG udpmsg)
{
	sendto(m_udpSocketGui, (char*)&udpmsg, sizeof(udpmsg), 0, (struct sockaddr *)&m_udpServGui, m_udpSockLenGui);
}

void UdpSendToGui(char *buffer)
{
	sendto(m_udpSocketGui, buffer, sizeof(buffer), 0, (struct sockaddr *)&m_udpServGui, m_udpSockLenGui);
}

void UdpBindGui(int port)
{
	m_udpServGui.sin_family = AF_INET;
	m_udpServGui.sin_port = htons(port);
	m_udpServGui.sin_addr.s_addr = htonl(INADDR_ANY);
	m_udpSockLenGui = sizeof(m_udpServGui);

	memset((char*)&m_udpClientGui, 0x00, m_udpSockLenGui);

	bind(m_udpSocketGui, (struct sockaddr*)&m_udpServGui, m_udpSockLenGui);
}

int UdpRecvFromGui(char *buffer)
{
	return recvfrom(m_udpSocketGui, buffer, 512, 0, (struct sockaddr*)&m_udpClientGui, &m_udpSockLenGui);
}


void UdpInitKVM()
{
	m_udpSocketKVM0 = socket(AF_INET, SOCK_DGRAM, 0);
	m_udpSocketKVM1 = socket(AF_INET, SOCK_DGRAM, 0);
}

void UdpConnectKVM()
{
	m_udpServKVM0.sin_family = AF_INET;
	m_udpServKVM0.sin_port = htons(UDPPROC_KVM0_PORT);
	m_udpServKVM0.sin_addr.s_addr = inet_addr("127.0.0.1");
	m_udpSockLenKVM0 = sizeof(m_udpServKVM0);

	m_udpServKVM1.sin_family = AF_INET;
	m_udpServKVM1.sin_port = htons(UDPPROC_KVM1_PORT);
	m_udpServKVM1.sin_addr.s_addr = inet_addr("127.0.0.1");
	m_udpSockLenKVM1 = sizeof(m_udpServKVM1);
}

void UdpSendKVM(UDPMSG udpmsg)
{
	sendto(m_udpSocketKVM0, (char*)&udpmsg, sizeof(udpmsg), 0, (struct sockaddr *)&m_udpServKVM0, m_udpSockLenKVM0);
	sendto(m_udpSocketKVM1, (char*)&udpmsg, sizeof(udpmsg), 0, (struct sockaddr *)&m_udpServKVM1, m_udpSockLenKVM1);
}