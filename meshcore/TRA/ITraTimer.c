#include "ITraTimer.h"

#include "meshcore/TRA/ITraCore.h"
#include "meshcore/GUI/IGuiProcess.h"
#include "meshcore/TRA/ITraUtil.h"

extern BOOL g_timer_shutdown;
extern BOOL g_timer_use;
extern int g_sender_delay;
extern int g_supervisor_delay;
extern int g_update_check_delay;

extern char g_sender_path[_MAX_PATH + 40];
extern char g_sender_update_path[_MAX_PATH + 40];
extern char g_supervisor_path[_MAX_PATH + 40];
extern char g_supervisor_update_path[_MAX_PATH + 40];
extern char g_updater_path[_MAX_PATH + 40];
extern char g_updater_update_path[_MAX_PATH + 40];

extern BOOL g_timer_session_change;
extern int g_external_update_timer_count;
extern BOOL g_me_version;

void startMETimer() 
{
	m_ThreadTimer = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(DoThreadTimer), (LPVOID)NULL, 0, &threadTimerID);
	ResumeThread(m_ThreadTimer);
}

LRESULT CALLBACK DoThreadTimer(LPVOID lpParameter)
{
	int delay_count = 0;

	while (!g_timer_shutdown)
	{
		Sleep(1000);
		if (!g_timer_use || !g_me_version)
		{
			delay_count = 0;
			continue;
		}

		if (g_external_update_timer_count > 0)
		{
			delay_count = 0;
			g_external_update_timer_count--;
			continue;
		}

		delay_count++;

		if ((delay_count % g_update_check_delay == 0))
		{
			renameFile(g_sender_update_path, g_sender_path, FALSE);
			renameFile(g_supervisor_update_path, g_supervisor_path, FALSE);
			renameFile(g_updater_update_path, g_updater_path, FALSE);
		}
		
		if ((delay_count % g_supervisor_delay == 0) && !SearchProcessByName("supervisor.exe"))
		{
			if (g_timer_session_change)
			{
				g_timer_session_change = FALSE;
			}
			else 
			{
				RunSupervisor();
			}
		}

		if (delay_count == g_sender_delay && !SearchProcessByName("sender.exe"))
		{
			RunSender();
		}

		if (delay_count >= g_sender_delay)
			delay_count = 0;
	}
	//MessageBox(NULL, TEXT("ITraTimer DoThreadTimer run."), TEXT("TRA Agent"), MB_OK | MB_ICONERROR);

	return 0L;
}