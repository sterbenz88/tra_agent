#include "ITraCore.h"

//////////////////////// DEBUG //////////////////////

BOOL g_file_log = FALSE;

/////////////////////////////////////////////////////

HINSTANCE g_hres = NULL;
int g_clientCount = 0;
BOOL g_serviceMode = TRUE;
int g_socketNumber = 0;

// 20190416 pjh
BOOL g_testMode = FALSE;
char g_testAgentName[255];

// 20190511 pjh
BOOL g_commandFlag = FALSE;

// 20190512 pjh
BOOL g_remoteUserAllowed = FALSE;
BOOL g_showRedDot = TRUE;

// 20190729
int g_kvm_type = 0;
BOOL g_isCallForceDisconnect = FALSE;

// 20191018
BOOL g_timer_shutdown = FALSE;
BOOL g_timer_use = TRUE;
BOOL g_timer_session_change = FALSE;
int g_sender_delay = 60;				// Second
int g_supervisor_delay = 20;			// Second
int g_update_check_delay = 5;			// Second

char g_sender_path[_MAX_PATH + 40];
char g_sender_update_path[_MAX_PATH + 40];
char g_supervisor_path[_MAX_PATH + 40];
char g_supervisor_update_path[_MAX_PATH + 40];
char g_updater_path[_MAX_PATH + 40];
char g_updater_update_path[_MAX_PATH + 40];

int g_external_update_timer_count = 0;

////////////////////////////////////////////////////////////////////////////

BOOL g_me_version = FALSE;