#pragma once
#include "microstack/ILibParsers.h"
#include "microstack/ILibAsyncSocket.h"

typedef struct TRA_CLIENT {
	ILibAsyncSocket_SocketModule socketModule;
	int socketNumber;
	char ip_addr[20];
	char com_name[50];
	struct TRA_CLIENT* next;
}TRA_CLIENT;

enum tra_client_command {
	TRA_CLIENT_COMMAND_ADD = 0,
	TRA_CLIENT_COMMAND_DEL
} m_traClientCommand;

TRA_CLIENT *traclient_head;
int tra_size;

void initTraClient();
void addClient(ILibAsyncSocket_SocketModule socketModule, int socketNumber, char ip_addr[20], char com_name[50]);
void deleteClientBySocketNumber(int socketNumber);
int deleteClientBySocketModule(ILibAsyncSocket_SocketModule socketModule);
int setDataClient(ILibAsyncSocket_SocketModule socketModule, char ip_addr[20], char com_name[50]/*, UDPMSG udpmsg*/);
void printTraClient();