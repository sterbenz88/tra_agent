#pragma once
#include <winsock2.h>
#include <stdio.h>
#include <string.h>
#include <ws2tcpip.h>

void InitCursorShapeDetector();

HCURSOR getHCursor();
BOOL isCursorShapeChanged();
HCURSOR getLastCursor();
long GetCursorSendData(HCURSOR hCursor);
void DestroyCursorDetector();

LRESULT CALLBACK DoCursorShapeDetetorLoop(LPVOID lpParameter);

HANDLE hCursorDetectThread;
DWORD threadCommunicatorID;

HCURSOR m_lastHCursor;
BOOL g_shutdown;

HCURSOR cursorARROW, cursorIBEAM, cursorWAIT, cursorCROSS, cursorUPARROW, cursorSIZE, cursorICON
, cursorSIZENWSE, cursorSIZENESW, cursorSIZEWE, cursorSIZENS, cursorSIZEALL
, cursorNO, cursorHAND, cursorAPPSTARTING, cursorHELP;