#include "ITraSessionList.h"

void initTraClient() {

	traclient_head = NULL;
	tra_size = 0;
}

void addClient(ILibAsyncSocket_SocketModule socketModule, int socketNumber, char ip_addr[20], char com_name[50]) {

	TRA_CLIENT *newClient = (TRA_CLIENT *)malloc(sizeof(TRA_CLIENT));

	newClient->socketModule = socketModule;
	newClient->socketNumber = socketNumber;
	strcpy_s(newClient->ip_addr, sizeof(newClient->ip_addr), ip_addr);
	strcpy_s(newClient->com_name, sizeof(newClient->com_name), com_name);

	if (traclient_head == NULL)
	{
		traclient_head = newClient;
		traclient_head->next = NULL;
	}
	else
	{
		newClient->next = traclient_head;
		traclient_head = newClient;
	}

	tra_size++;
}

int setDataClient(ILibAsyncSocket_SocketModule socketModule, char ip_addr[20], char com_name[50]/*, UDPMSG udpmsg*/) {

	int targetSocketNumber = -1;

	if (traclient_head == NULL) {
		return targetSocketNumber;
	}

	TRA_CLIENT *cur_client = traclient_head;
	while (cur_client != NULL)
	{
		if (cur_client->socketModule == socketModule)
		{
			strcpy_s(cur_client->com_name, sizeof(cur_client->com_name), ip_addr);
			strcpy_s(cur_client->ip_addr, sizeof(cur_client->ip_addr), com_name);

			targetSocketNumber = cur_client->socketNumber;

			return targetSocketNumber;
		}

		cur_client = cur_client->next;
	}

	return targetSocketNumber;
}

int deleteClientBySocketModule(ILibAsyncSocket_SocketModule socketModule) {

	int result_value = -1;

	if (traclient_head == NULL) {
		tra_size = 0;
		return result_value;
	}

	TRA_CLIENT *cur_client = traclient_head;
	TRA_CLIENT *pre_client = NULL;
	TRA_CLIENT *del_client = NULL;
	while (cur_client != NULL)
	{
		if (cur_client->socketModule == socketModule)
		{
			del_client = cur_client;
			result_value = cur_client->socketNumber;

			// Head
			if (pre_client == NULL)
			{
				// One Item
				if (traclient_head->next = NULL)
				{
					traclient_head = NULL;
					tra_size = 0;
				}
				else
				{
					traclient_head = traclient_head->next;
				}
			}
			else
			{
				// head... item1 -> item2 -> item3
				// head... item1 -> item3
				pre_client->next = cur_client->next;
			}

			tra_size--;
			if (tra_size < 0) tra_size = 0;

			if (del_client != NULL)
				free(del_client);

			return result_value;
		}

		pre_client = cur_client;
		cur_client = cur_client->next;
	}

	return result_value;
}

void deleteClientBySocketNumber(int socketNumber) {

	if (traclient_head == NULL) {
		tra_size = 0;
		return;
	}

	TRA_CLIENT *cur_client = traclient_head;
	TRA_CLIENT *pre_client = NULL;
	TRA_CLIENT *del_client = NULL;
	while (cur_client != NULL)
	{
		if (cur_client->socketNumber == socketNumber)
		{
			del_client = cur_client;

			// Head
			if (pre_client == NULL)
			{
				// One Item
				if (traclient_head->next = NULL)
				{
					traclient_head = NULL;
					tra_size = 0;
				}
				else
				{
					traclient_head = traclient_head->next;
				}
			}
			else
			{
				// head... item1 -> item2 -> item3
				// head... item1 -> item3
				pre_client->next = cur_client->next;
			}

			tra_size--;
			if (tra_size < 0) tra_size = 0;

			if (del_client != NULL)
				free(del_client);

			return;
		}

		pre_client = cur_client;
		cur_client = cur_client->next;
	}
}

void printTraClient() {

	TRA_CLIENT *temp_client = traclient_head;
	while (temp_client != NULL) {

		//char msg[128];
		//sprintf(msg, "printTraClient : %d\n", temp_client->socketNumber);
		//printf(msg);
		//MessageBox(NULL, msg, TEXT("TRA Agent"), MB_OK | MB_ICONERROR);

		temp_client = temp_client->next;
	}
}