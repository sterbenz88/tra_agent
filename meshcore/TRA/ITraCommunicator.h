#pragma once
#include <winsock2.h>
#include <stdio.h>
#include <string.h>
#include <ws2tcpip.h>

#define COMMUNICATOR_MAIN_PORT 47685

#define MAX_SOCK_COUNT 30

#define BUFSIZE 1024

typedef struct _communicatormsg {
	int command;
} COMMUNICATOR_MSG;

enum communicator_command {
	COMMUNICATOR_TEMP = 0
} m_communicatorCommand;

int m_communicatorServSocket, m_communicatorClientSocket;
struct sockaddr_in m_communicatorServAddr, m_communicatorClientAddr;
socklen_t m_communicatorServSockLen, m_communicatorClientSockLen;

LRESULT CALLBACK DoCommunicatorServRecv(LPVOID lpParameter);

void InitCommunicator();
void ErrorHandling(char *message);