#include "ITraFileLog.h"
#include <stdio.h>

#include "meshcore/TRA/ITraCore.h"

extern BOOL g_file_log;

char g_fileLogText[100];

FILE* fp;
char szFileName[20];
int filenum;
int linenum;
CRITICAL_SECTION log_cs;

void initFileLog()
{
	if (!g_file_log)
		return;

	mkdir(LOG_DIR);
	memset(szFileName, 0x00, 20);
	InitializeCriticalSection(&log_cs);
	fp = NULL;
}

void startFileLog(char* fileName)
{
	if (!g_file_log)
		return;

	if (fp != NULL) fclose(fp);
	strcpy(szFileName, fileName);

	SYSTEMTIME current_time;
	GetLocalTime(&current_time);

	char filename[100];
	sprintf(filename, "%s[%.4ld%.2ld%.2ld %.2ld%.2ld%.2ld].txt", szFileName, current_time.wYear, current_time.wMonth, current_time.wDay,
		current_time.wHour, current_time.wMinute, current_time.wSecond);
	fp = fopen(filename, "w");
	linenum = 0;
}

void writeFileLog(char* msg)
{
	if (!g_file_log)
		return;

	if (fp == NULL) return;

	EnterCriticalSection(&log_cs);
	SYSTEMTIME current_time;
	GetLocalTime(&current_time);

	fprintf(fp, "[%ld/%ld/%ld %ld:%ld:%ld] %s\n", current_time.wYear, current_time.wMonth, current_time.wDay,
		current_time.wHour, current_time.wMinute, current_time.wSecond,
		msg);
	fflush(fp);
	linenum++;
	//	if(linenum > 50000) OpenLogFile(m_lpszFileName);
	if (linenum > 30000) startFileLog(szFileName);
	LeaveCriticalSection(&log_cs);
}

void releaseFileLog()
{
	if (!g_file_log)
		return;

	DeleteCriticalSection(&log_cs);
}