#include "ITraCursorShapeDetector.h"
#include "ITraUdpProc.h"

void InitCursorShapeDetector()
{
	m_lastHCursor = 0;
	g_shutdown = FALSE;

	cursorARROW = LoadCursor(NULL, IDC_ARROW);
	cursorIBEAM = LoadCursor(NULL, IDC_IBEAM);
	cursorWAIT = LoadCursor(NULL, IDC_WAIT);
	cursorCROSS = LoadCursor(NULL, IDC_CROSS);
	cursorUPARROW = LoadCursor(NULL, IDC_UPARROW);
	cursorSIZE = LoadCursor(NULL, IDC_SIZE);
	cursorICON = LoadCursor(NULL, IDC_ICON);
	cursorSIZENWSE = LoadCursor(NULL, IDC_SIZENWSE);
	cursorSIZENESW = LoadCursor(NULL, IDC_SIZENESW);
	cursorSIZEWE = LoadCursor(NULL, IDC_SIZEWE);
	cursorSIZENS = LoadCursor(NULL, IDC_SIZENS);
	cursorSIZEALL = LoadCursor(NULL, IDC_SIZEALL);
	cursorNO = LoadCursor(NULL, IDC_NO);
	cursorHAND = LoadCursor(NULL, IDC_HAND);
	cursorAPPSTARTING = LoadCursor(NULL, IDC_APPSTARTING);
	cursorHELP = LoadCursor(NULL, IDC_HELP);

	hCursorDetectThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(DoCursorShapeDetetorLoop), (LPVOID)NULL, 0, &threadCommunicatorID);
	ResumeThread(hCursorDetectThread);
}

HCURSOR getHCursor()
{
	CURSORINFO cursorInfo;
	cursorInfo.cbSize = sizeof(CURSORINFO);

	if (GetCursorInfo(&cursorInfo) == 0) {
		return FALSE;
	}

	return cursorInfo.hCursor;
}

BOOL isCursorShapeChanged() 
{
	HCURSOR hCursor = getHCursor();

	if (hCursor == m_lastHCursor) {
		return FALSE;
	}
	m_lastHCursor = hCursor;

	return TRUE;
}

long GetCursorSendData(HCURSOR hCursor)
{
	if (hCursor == cursorARROW)		return (long)IDC_ARROW;
	else if (hCursor == cursorIBEAM)	return (long)IDC_IBEAM;
	else if (hCursor == cursorWAIT)	return (long)IDC_WAIT;
	else if (hCursor == cursorCROSS)	return (long)IDC_CROSS;
	else if (hCursor == cursorUPARROW)	return (long)IDC_UPARROW;
	else if (hCursor == cursorSIZE)	return (long)IDC_SIZE;
	else if (hCursor == cursorICON)	return (long)IDC_ICON;
	else if (hCursor == cursorSIZENWSE)return (long)IDC_SIZENWSE;
	else if (hCursor == cursorSIZENESW)return (long)IDC_SIZENESW;
	else if (hCursor == cursorSIZEWE)	return (long)IDC_SIZEWE;
	else if (hCursor == cursorSIZENS)	return (long)IDC_SIZENS;
	else if (hCursor == cursorSIZEALL)	return (long)IDC_SIZEALL;
	else if (hCursor == cursorNO)		return (long)IDC_NO;
	else if (hCursor == cursorHAND)	return (long)IDC_HAND;
	else if (hCursor == cursorAPPSTARTING) return (long)IDC_APPSTARTING;
	else if (hCursor == cursorHELP)	return (long)IDC_HELP;
	else							return (long)IDC_ARROW;
}

HCURSOR getLastCursor()
{
	return m_lastHCursor;
}

void DestroyCursorDetector()
{
	g_shutdown = TRUE;
}

LRESULT CALLBACK DoCursorShapeDetetorLoop(LPVOID lpParameter)
{
	// For Test
	//UdpInitGui();
	//UdpConnectGui(UDPPROC_GUI_PORT);

	UDPMSG udpmsg;

	while (!g_shutdown)
	{
		BOOL isCursorChanged;
		isCursorChanged = isCursorShapeChanged();
	
		if (isCursorChanged)
		{
			long cursorid = GetCursorSendData(m_lastHCursor);

			memset((void*)&udpmsg, 0x00, sizeof(udpmsg));
			udpmsg.command = UDP_GUI_CURSOR_CHANGED;

			udpmsg.type = cursorid;

			UdpSend(udpmsg);
			//UdpSendGui(udpmsg);
		}

		Sleep(100);
	}

	return 0L;
}
