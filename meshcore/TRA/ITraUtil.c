#include "ITraUtil.h"
#include <shlobj.h>

void writeINIFile(char* app_name, char* key, char* content)
{
	char targetini[_MAX_PATH + 40];
	size_t targetinilen = 0;

	if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targetini) != S_FALSE)
	{
		targetinilen = strnlen_s(targetini, _MAX_PATH + 40);
		if (targetinilen <= MAX_PATH) memcpy_s(targetini + targetinilen, _MAX_PATH + 40 - targetinilen, "\\TRA Agent\\agent.ini", 38);
		//targetexelen += 25;
	}

	WritePrivateProfileString(app_name, key, content, targetini);
}

void renameFile(char* source, char* target, BOOL failIfExist)
{
	if (access(source, 0) < 0)
	{
		return;
	}

	if (CopyFile(source, target, failIfExist))
	{
		DeleteFile(source);
	}
}

void setFullFilePath(char* result_text)
{
	char targettext[_MAX_PATH + 40];
	size_t targettextlen = 0;

	if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targettext) != S_FALSE)
	{
		targettextlen = strnlen_s(targettext, _MAX_PATH + 40);
		if (targettextlen <= MAX_PATH)
		{
			memcpy_s(targettext + targettextlen, _MAX_PATH + 40 - targettextlen, "\\TRA Agent\\", 38);
			strcat(targettext, result_text);

			strcpy_s(result_text, _MAX_PATH + 40, targettext);
		}
	}
}