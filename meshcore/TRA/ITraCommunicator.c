#include "ITraCommunicator.h"


int c_socket_list[MAX_SOCK_COUNT] = { 0 };
int c_client_count = 0;
int c_socket_number = 0;

HANDLE hCommunicatorThread;

void ErrorHandling(char *message) {

#ifdef TRA_DEBUG
	ILIBLOGMESSSAGE(message);
#endif

	//WSACleanup();
	//exit(1);
}

void InitCommunicator()
{
	m_communicatorServSocket = socket(PF_INET, SOCK_STREAM, 0);

	if (m_communicatorServSocket == INVALID_SOCKET) {
		ErrorHandling("Socket Error...\n");
		return;
	}

	memset(&m_communicatorServAddr, 0, sizeof(SOCKADDR_IN));
	m_communicatorServAddr.sin_family = AF_INET;
	m_communicatorServAddr.sin_port = htons(COMMUNICATOR_MAIN_PORT);
	m_communicatorServAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(m_communicatorServSocket, (void *)&m_communicatorServAddr, sizeof(m_communicatorServAddr)) == SOCKET_ERROR) {
		ErrorHandling("Bind Error...\n");
		return;
	}

	if (listen(m_communicatorServSocket, 5) == -1) {
		ErrorHandling("Listen Error...\n");
		return;
	}

	m_communicatorClientSockLen = sizeof(m_communicatorClientAddr);

	while (1)
	{
		m_communicatorClientSocket = accept(m_communicatorServSocket, (struct sockaddr_in*)&m_communicatorClientAddr, &m_communicatorClientSockLen);

		if (c_client_count == MAX_SOCK_COUNT)
		{
			close(m_communicatorClientSocket);
			continue;
		}	
		if (m_communicatorClientSocket < 0)
		{
			continue;
		}

		c_socket_list[c_socket_number++] = m_communicatorClientSocket;
		if (c_socket_number >= MAX_SOCK_COUNT)
			c_socket_number = 0;

		c_client_count++;

		hCommunicatorThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)DoCommunicatorServRecv, (void *)m_communicatorClientSocket, 0, NULL);
		CloseHandle(hCommunicatorThread);
	}

	close(m_communicatorServSocket);
}

LRESULT CALLBACK DoCommunicatorServRecv(LPVOID lpParameter)
{
	int clientSocket = (int)lpParameter;

	char messageBuffer[BUFSIZE];
	int receiveBytes;
	while (receiveBytes = recv(clientSocket, messageBuffer, BUFSIZE, 0))
	{
		if (receiveBytes > 0)
		{
			//printf("TRACE - Receive message : %s (%d bytes)\n", messageBuffer, receiveBytes);
			
			// 5-2. 데이터 쓰기 (Echo)
			int sendBytes = send(clientSocket, messageBuffer, strlen(messageBuffer), 0);
			if (sendBytes > 0)
			{
				//printf("TRACE - Send message : %s (%d bytes)\n", messageBuffer, sendBytes);
			}
		}
		else
		{
			break;
		}
	}

	c_client_count--;
	if (c_client_count < 0)
		c_client_count = 0;

	closesocket(clientSocket);
	return 0;
}

/*
void UdpInit()
{
	m_udpSocket = socket(AF_INET, SOCK_DGRAM, 0);
}

void UdpConnect(int port)
{
	m_udpServ.sin_family = AF_INET;
	m_udpServ.sin_port = htons(port);
	m_udpServ.sin_addr.s_addr = inet_addr("127.0.0.1");
	m_udpSockLen = sizeof(m_udpServ);
}

void UdpSend(UDPMSG udpmsg)
{
	sendto(m_udpSocket, (char*)&udpmsg, sizeof(udpmsg), 0, (struct sockaddr *)&m_udpServ, m_udpSockLen);
}

void UdpSendTo(char *buffer)
{
	sendto(m_udpSocket, buffer, sizeof(buffer), 0, (struct sockaddr *)&m_udpServ, m_udpSockLen);
}

void UdpBind(int port)
{
	m_udpServ.sin_family = AF_INET;
	m_udpServ.sin_port = htons(port);
	m_udpServ.sin_addr.s_addr = htonl(INADDR_ANY);
	m_udpSockLen = sizeof(m_udpServ);

	memset((char*)&m_udpClient, 0x00, m_udpSockLen);

	bind(m_udpSocket, (struct sockaddr*)&m_udpServ, m_udpSockLen);
}
*/
