#include <Windows.h>

#define LOG_DIR					"C:\\Test"
#define LOG_FULL_DIR			"C:\\Test\\dlog"

void initFileLog();
void startFileLog(char* fileName);
void writeFileLog(char* msg);
void releaseFileLog();