#include <UserEnv.h> 
#pragma comment(lib,"UserEnv.lib")
#include <WtsApi32.h>
#include <tlhelp32.h>
#include <shlobj.h>

typedef NTSTATUS(NTAPI *_NtQueryInformationProcess)(
	HANDLE ProcessHandle,
	DWORD ProcessInformationClass,
	PVOID ProcessInformation,
	DWORD ProcessInformationLength,
	PDWORD ReturnLength
	);

typedef struct _UNICODE_STRING
{
	USHORT Length;
	USHORT MaximumLength;
	PWSTR Buffer;
} UNICODE_STRING, *PUNICODE_STRING;

typedef struct _PROCESS_BASIC_INFORMATION
{
	LONG ExitStatus;
	PVOID PebBaseAddress;
	ULONG_PTR AffinityMask;
	LONG BasePriority;
	ULONG_PTR UniqueProcessId;
	ULONG_PTR ParentProcessId;
} PROCESS_BASIC_INFORMATION, *PPROCESS_BASIC_INFORMATION;

HANDLE GetToken(BOOL isWinlogon);
BOOL RunAsUserSelf(char* cmd, BOOL isWinlogon);

BOOL SearchProcessByName(char* processname);
BOOL TerminateProcessByName(char* szFilename);
HANDLE GetProcessHandleByName(char* szFilename);

BOOL TerminateSlaveProcess();
HANDLE GetProcessHandleByPID(int pid);
BOOL IsSlaveProcess(int pid);

//////

void RunGuiMeshAgent();
void RunAllowRemoteMeshAgent(int socketNumber);
void RunConsoleMeshAgent();

void RunSupervisor();
void RunSender();
void RunUpdater();