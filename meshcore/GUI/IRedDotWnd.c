#include "IRedDotWnd.h"
#include "meshcore/TRA/ITraCore.h"

extern HMODULE g_hres;

enum TASKBARPOS { TASKBAR_LEFT_BOTTOM = 0, TASKBAR_RIGHT, TASKBAR_TOP };

const UINT TRANSPARENT_WINDOW_TOP_LEFT = 0;
const UINT TRANSPARENT_WINDOW_TOP = 1;
const UINT TRANSPARENT_WINDOW_TOP_RIGHT = 2;
const UINT TRANSPARENT_WINDOW_BOTTOM_LEFT = 3;
const UINT TRANSPARENT_WINDOW_BOTTOM = 4;
const UINT TRANSPARENT_WINDOW_BOTTOM_RIGHT = 5;

_ScreenInfo __stScreenInfo;
int g_posType = 2;

HWND m_hwndRC1;
HWND m_hwndRC2;

RECT m_rectRC1;
RECT m_rectRC2;

RECT m_rectRC1Temp1;
RECT m_rectRC2Temp1;

HFONT m_font;

BOOL isShow = FALSE;

void red_test(int i)
{

}

void red_dot_init()
{
	m_font = CreateFont(11, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, "Gulim");
}

void get_system_info()
{
	memset((void*)&__stScreenInfo, 0x00, sizeof(_ScreenInfo));
	for (int i = 0; i < 5; i++)
	{
		memset((void*)&__stScreenInfo.devMode[i], 0x00, sizeof(DEVMODE));
	}

	SystemParametersInfo(SPI_GETWORKAREA, 0, &__stScreenInfo.rcDesktop, 0);
	__stScreenInfo.nDesktopWidth = __stScreenInfo.rcDesktop.right - __stScreenInfo.rcDesktop.left;
	__stScreenInfo.nDesktopHeight = __stScreenInfo.rcDesktop.bottom - __stScreenInfo.rcDesktop.top;
	__stScreenInfo.nScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	__stScreenInfo.nScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	BOOL bTaskbarOnRight = __stScreenInfo.nDesktopWidth < __stScreenInfo.nScreenWidth && __stScreenInfo.rcDesktop.left == 0;
	BOOL bTaskbarOnLeft = __stScreenInfo.nDesktopWidth < __stScreenInfo.nScreenWidth && __stScreenInfo.rcDesktop.left != 0;
	BOOL bTaskBarOnTop = __stScreenInfo.nDesktopHeight < __stScreenInfo.nScreenHeight && __stScreenInfo.rcDesktop.top != 0;
	BOOL bTaskbarOnBottom = __stScreenInfo.nDesktopHeight < __stScreenInfo.nScreenHeight && __stScreenInfo.rcDesktop.top == 0;
	__stScreenInfo.bGetTaskBarPos = 1;

	if (bTaskbarOnRight)
		__stScreenInfo.nTaskBarPos = TASKBAR_RIGHT;
	else if (bTaskBarOnTop)
		__stScreenInfo.nTaskBarPos = TASKBAR_TOP;
	else
		__stScreenInfo.nTaskBarPos = TASKBAR_LEFT_BOTTOM;

	__stScreenInfo.bGetDualMonitorInfo = 1;

	DISPLAY_DEVICE displayDevice;
	displayDevice.cb = sizeof(displayDevice);

	DEVMODE devMode;
	devMode.dmSize = sizeof(DEVMODE);

	BOOL bFindLoop = TRUE;
	int DvNum = 1;

	int nDeviceIndex = 0;

	while (1)
	{
		bFindLoop = EnumDisplayDevices(NULL, nDeviceIndex++, &displayDevice, 0);
		if (!bFindLoop)
		{
			break;
		}

		if (EnumDisplaySettings(displayDevice.DeviceName, ENUM_CURRENT_SETTINGS, &devMode))
		{
			if (devMode.dmDisplayFrequency != 0)
			{
				if (__stScreenInfo.nMonitorCount < 5)
				{
					memcpy((void*)&__stScreenInfo.devMode[__stScreenInfo.nMonitorCount], (void*)&devMode, sizeof(DEVMODE));
					__stScreenInfo.nMonitorCount++;
				}
			}
			DvNum++;
		}
	}

	if (__stScreenInfo.nMonitorCount > 1)
	{
		if (__stScreenInfo.devMode[0].dmPosition.x < 0)
		{
			__stScreenInfo.nDevideLinePos_x = __stScreenInfo.devMode[0].dmPosition.x + __stScreenInfo.devMode[0].dmPelsWidth - 2;
			__stScreenInfo.nTextPos_x = __stScreenInfo.nDevideLinePos_x;
		}
		else if (__stScreenInfo.devMode[1].dmPosition.x < 0)
		{
			__stScreenInfo.nDevideLinePos_x = __stScreenInfo.devMode[1].dmPosition.x + __stScreenInfo.devMode[1].dmPelsWidth - 2;
			__stScreenInfo.nTextPos_x = __stScreenInfo.nDevideLinePos_x;
		}
		else
		{
			__stScreenInfo.nDevideLinePos_x = __stScreenInfo.nScreenWidth;
			__stScreenInfo.nTextPos_x = __stScreenInfo.devMode[0].dmPelsWidth + __stScreenInfo.devMode[1].dmPelsWidth;
		}
	}
}

void set_window_rect(int posType)
{
	g_posType = posType;
	if (posType == TRANSPARENT_WINDOW_TOP_RIGHT)
	{
		m_rectRC1.left = __stScreenInfo.rcDesktop.right - 250;
		m_rectRC1.top = __stScreenInfo.rcDesktop.top + 8;
		m_rectRC1.right = m_rectRC1.left + 240;
		m_rectRC1.bottom = m_rectRC1.top + 60;

		if (__stScreenInfo.nMonitorCount > 1)
		{
			m_rectRC2.left = __stScreenInfo.nTextPos_x - 270;
			m_rectRC2.top = __stScreenInfo.rcDesktop.top + 8;
			m_rectRC2.right = m_rectRC2.left + 260;
			m_rectRC2.bottom = m_rectRC2.top + 60;
		}
	}
	else if (posType == TRANSPARENT_WINDOW_TOP_LEFT)
	{
		m_rectRC1.left = __stScreenInfo.rcDesktop.left + 10;
		m_rectRC1.top = __stScreenInfo.rcDesktop.top;
		m_rectRC1.right = m_rectRC1.left + 240;
		m_rectRC1.bottom = m_rectRC1.top + 60;

		if (__stScreenInfo.nMonitorCount > 1)
		{
			m_rectRC2.left = __stScreenInfo.nDevideLinePos_x + 10;
			m_rectRC2.top = __stScreenInfo.rcDesktop.top;
			m_rectRC2.right = m_rectRC2.left + 260;
			m_rectRC2.bottom = m_rectRC2.top + 60;
		}
	}
	else if (posType == TRANSPARENT_WINDOW_BOTTOM_RIGHT)
	{
		m_rectRC1.left = __stScreenInfo.rcDesktop.right - 250;
		m_rectRC1.top = __stScreenInfo.rcDesktop.bottom - 80;
		m_rectRC1.right = m_rectRC1.left + 240;
		m_rectRC1.bottom = m_rectRC1.top + 60;

		if (__stScreenInfo.nMonitorCount > 1)
		{
			m_rectRC2.left = __stScreenInfo.nTextPos_x - 270;
			m_rectRC2.top = __stScreenInfo.rcDesktop.bottom - 80;
			m_rectRC2.right = m_rectRC2.left + 260;
			m_rectRC2.bottom = m_rectRC2.top + 60;
		}
	}
	else if (posType == TRANSPARENT_WINDOW_BOTTOM_LEFT)
	{
		m_rectRC1.left = __stScreenInfo.rcDesktop.left + 10;
		m_rectRC1.top = __stScreenInfo.rcDesktop.bottom - 80;
		m_rectRC1.right = m_rectRC1.left + 240;
		m_rectRC1.bottom = m_rectRC1.top + 60;

		if (__stScreenInfo.nMonitorCount > 1)
		{
			m_rectRC2.left = __stScreenInfo.nDevideLinePos_x + 10;
			m_rectRC2.top = __stScreenInfo.rcDesktop.bottom - 80;
			m_rectRC2.right = m_rectRC2.left + 260;
			m_rectRC2.bottom = m_rectRC2.top + 60;
		}
	}
	else if (posType == TRANSPARENT_WINDOW_TOP)
	{
		m_rectRC1.left = __stScreenInfo.rcDesktop.right / 2 - 120;
		m_rectRC1.top = __stScreenInfo.rcDesktop.top;
		m_rectRC1.right = m_rectRC1.left + 240;
		m_rectRC1.bottom = m_rectRC1.top + 60;

		if (__stScreenInfo.nMonitorCount > 1)
		{
			m_rectRC2.left = __stScreenInfo.nDevideLinePos_x + ((__stScreenInfo.nTextPos_x - __stScreenInfo.nDevideLinePos_x) / 2) - 120;
			m_rectRC2.top = __stScreenInfo.rcDesktop.top;
			m_rectRC2.right = m_rectRC2.left + 260;
			m_rectRC2.bottom = m_rectRC2.top + 60;
		}
	}
	else if (posType == TRANSPARENT_WINDOW_BOTTOM)
	{
		m_rectRC1.left = __stScreenInfo.rcDesktop.right / 2 - 120;
		m_rectRC1.top = __stScreenInfo.rcDesktop.bottom - 80;
		m_rectRC1.right = m_rectRC1.left + 240;
		m_rectRC1.bottom = m_rectRC1.top + 60;

		if (__stScreenInfo.nMonitorCount > 1)
		{
			m_rectRC2.left = __stScreenInfo.nDevideLinePos_x + ((__stScreenInfo.nTextPos_x - __stScreenInfo.nDevideLinePos_x) / 2) - 130;
			m_rectRC2.top = __stScreenInfo.rcDesktop.bottom - 80;
			m_rectRC2.right = m_rectRC2.left + 260;
			m_rectRC2.bottom = m_rectRC2.top + 60;
		}
	}
	else
	{
		// Default = TOP_RIGHT
		m_rectRC1.left = __stScreenInfo.rcDesktop.right - 250;
		m_rectRC1.top = __stScreenInfo.rcDesktop.top + 8;
		m_rectRC1.right = m_rectRC1.left + 240;
		m_rectRC1.bottom = m_rectRC1.top + 60;

		if (__stScreenInfo.nMonitorCount > 1)
		{
			m_rectRC2.left = __stScreenInfo.nTextPos_x - 270;
			m_rectRC2.top = __stScreenInfo.rcDesktop.top + 8;
			m_rectRC2.right = m_rectRC2.left + 260;
			m_rectRC2.bottom = m_rectRC2.top + 60;
		}
	}
}

void move_rc1_wnd(int x, int y)
{
	if (m_hwndRC1 != NULL)
	{
		SetWindowPos(m_hwndRC1, NULL, x, y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
	}
}

void move_rc2_wnd(int x, int y)
{
	if (m_hwndRC2 != NULL)
	{
		SetWindowPos(m_hwndRC2, NULL, x, y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
	}
}

void create_rc_wnds()
{
	isShow = FALSE;
	red_dot_init();
	get_system_info();
	set_window_rect(TRANSPARENT_WINDOW_TOP_RIGHT);
	create_rc1_wnd();
	if (__stScreenInfo.nMonitorCount > 1)
	{
		create_rc2_wnd();
	}
}


void create_rc1_wnd()
{
	WNDCLASSEX wndclass;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = wnd_rc1;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	//wndclass.hInstance = GetModuleHandle(NULL);
	wndclass.hInstance = g_hres;
	wndclass.hIcon = 0;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName = (const TCHAR *)NULL;
	wndclass.lpszClassName = "TRARC1";
	wndclass.hIconSm = 0;
	RegisterClassEx(&wndclass);

	m_hwndRC1 = CreateWindowEx(
		WS_EX_TOPMOST | WS_EX_TOOLWINDOW | WS_EX_LAYERED | WS_EX_TRANSPARENT,
		wndclass.lpszClassName,
		NULL,
		WS_POPUP | WS_OVERLAPPED,
		m_rectRC1.left,
		m_rectRC1.top,
		240,
		80,
		NULL,
		NULL,
		g_hres,
		NULL);

	if (m_hwndRC1 == NULL)
	{
		return;
	}

	//SetWindowLong(m_hwndRC1, GWL_USERDATA, (LONG) this);

	ShowWindow(m_hwndRC1, SW_HIDE);
}

void create_rc2_wnd()
{
	WNDCLASSEX wndclass;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = wnd_rc2;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	//wndclass.hInstance = GetModuleHandle(NULL);
	wndclass.hInstance = g_hres;
	wndclass.hIcon = 0;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName = (const TCHAR *)NULL;
	wndclass.lpszClassName = "TRARC2";
	wndclass.hIconSm = 0;
	RegisterClassEx(&wndclass);

	m_hwndRC2 = CreateWindowEx(
		WS_EX_TOPMOST | WS_EX_TOOLWINDOW | WS_EX_LAYERED | WS_EX_TRANSPARENT,
		wndclass.lpszClassName,
		NULL,
		WS_POPUP | WS_OVERLAPPED,
		m_rectRC2.left,
		m_rectRC2.top,
		260,
		80,
		NULL,
		NULL,
		g_hres,
		NULL);

	if (m_hwndRC2 == NULL)
	{
		return;
	}

	//SetWindowLong(m_hwndRC2, GWL_USERDATA, (LONG) this);
	ShowWindow(m_hwndRC2, SW_HIDE);
}

void destroy_rc_wnds()
{
	isShow = FALSE;

	if (m_hwndRC1 != NULL)
	{
		DestroyWindow(m_hwndRC1);
	}

	if (m_hwndRC2 != NULL)
	{
		DestroyWindow(m_hwndRC2);
	}
}

void refresh_rc_wnds(int posType)
{
	get_system_info();
	set_window_rect(posType);
	move_rc1_wnd(m_rectRC1.left, m_rectRC1.top);
	move_rc2_wnd(m_rectRC2.left, m_rectRC2.top);
}

void show_rc_wnds(int posType)
{
	if (isShow)
		return;

	isShow = TRUE;
	refresh_rc_wnds(posType);

	if (m_hwndRC1 != NULL)
	{
		ShowWindow(m_hwndRC1, SW_SHOW);
		SetForegroundWindow(m_hwndRC1);
	}

	if (m_hwndRC2 != NULL)
	{
		ShowWindow(m_hwndRC2, SW_SHOW);
		SetForegroundWindow(m_hwndRC2);
	}
}

void hide_rc_wnds()
{
	isShow = FALSE;

	if (m_hwndRC1 != NULL)
	{
		ShowWindow(m_hwndRC1, SW_HIDE);
	}

	if (m_hwndRC2 != NULL)
	{
		ShowWindow(m_hwndRC2, SW_HIDE);
	}
}

LRESULT CALLBACK wnd_rc1(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;

	PAINTSTRUCT ps;
	typedef BOOL(WINAPI * SLWA)(HWND, COLORREF, BYTE, DWORD);

	switch (iMsg) {
	case WM_CREATE:
	{
		SLWA pSetLayeredWindowAttributes = NULL;
		HINSTANCE hmodUSER32 = LoadLibrary("USER32.DLL");
		if (hmodUSER32)
		{
			pSetLayeredWindowAttributes = (SLWA)GetProcAddress(hmodUSER32, "SetLayeredWindowAttributes");

			if (pSetLayeredWindowAttributes)
			{
				pSetLayeredWindowAttributes(hwnd, RGB(255, 255, 255), 0, LWA_COLORKEY);
			}
		}
	}
	return 0;

	case WM_PAINT:
	{
		hdc = BeginPaint(hwnd, &ps);

		//HFONT oldfont;
		//oldfont = (HFONT)SelectObject(hdc, m_font);
		//SetTextColor(hdc, RGB(255, 0, 0));

		HBRUSH oldBrush;
		HBRUSH hBrush;
		HPEN oldPen;
		HPEN hPen;

		hBrush = CreateSolidBrush(RGB(255, 0, 0));
		oldBrush = SelectObject(hdc, hBrush);

		hPen = CreatePen(PS_NULL, 0, RGB(255, 0, 0));
		oldPen = SelectObject(hdc, hPen);

		SetBkMode(hdc, TRANSPARENT);

		if (g_posType == TRANSPARENT_WINDOW_TOP_LEFT || g_posType == TRANSPARENT_WINDOW_BOTTOM_LEFT)
		{
			m_rectRC1Temp1;
			m_rectRC1Temp1.top = 0;
			m_rectRC1Temp1.left = 0;
			m_rectRC1Temp1.right = 240;
			m_rectRC1Temp1.bottom = 20;

			//DrawText(hdc, "��", lstrlen("��"), &m_rectRC1Temp1, DT_SINGLELINE | DT_LEFT | DT_VCENTER);
		}
		else if (g_posType == TRANSPARENT_WINDOW_TOP || g_posType == TRANSPARENT_WINDOW_BOTTOM)
		{

			m_rectRC1Temp1;
			m_rectRC1Temp1.top = 0;
			m_rectRC1Temp1.left = 0;
			m_rectRC1Temp1.right = 240;
			m_rectRC1Temp1.bottom = 20;

			//DrawText(hdc, "��", lstrlen("��"), &m_rectRC1Temp1, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		}
		else if (g_posType == TRANSPARENT_WINDOW_TOP_RIGHT || g_posType == TRANSPARENT_WINDOW_BOTTOM_RIGHT)
		{

			//m_rectRC1Temp1;
			//m_rectRC1Temp1.top = 0;
			//m_rectRC1Temp1.left = 0;
			//m_rectRC1Temp1.right = 240;
			//m_rectRC1Temp1.bottom = 20;

			//DrawText(hdc, "��", lstrlen("��"), &m_rectRC1Temp1, DT_SINGLELINE | DT_RIGHT | DT_VCENTER);
			//Ellipse(hdc, 40, 40, 40, 40);

			//RECT r = { m_rectRC1Temp1.top + 10, m_rectRC1Temp1.left + 10, 30, 30 };
			//FillRect(hdc, &r, hBrush);
			Ellipse(hdc, 235, 25, 225, 15);
		}

		//SelectObject(hdc, oldfont);
		SelectObject(hdc, oldPen);
		SelectObject(hdc, oldBrush);

		DeleteObject(oldBrush);
		DeleteObject(oldPen);

		ReleaseDC(hwnd, hdc);

		EndPaint(hwnd, &ps);
	}
	return 0;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

LRESULT CALLBACK wnd_rc2(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	typedef BOOL(WINAPI * SLWA)(HWND, COLORREF, BYTE, DWORD);

	switch (iMsg) {
	case WM_CREATE:
	{
		SLWA pSetLayeredWindowAttributes = NULL;
		HINSTANCE hmodUSER32 = LoadLibrary("USER32.DLL");
		if (hmodUSER32)
		{
			pSetLayeredWindowAttributes = (SLWA)GetProcAddress(hmodUSER32, "SetLayeredWindowAttributes");

			if (pSetLayeredWindowAttributes)
			{
				pSetLayeredWindowAttributes(hwnd, RGB(255, 255, 255), 0, LWA_COLORKEY);
			}
		}
	}
	return 0;

	case WM_PAINT:
	{
		hdc = BeginPaint(hwnd, &ps);
		
		//HFONT oldfont;
		//oldfont = (HFONT)SelectObject(hdc, m_font);
		//SetTextColor(hdc, RGB(255, 0, 0));
		
		HBRUSH oldBrush;
		HBRUSH hBrush;
		HPEN oldPen;
		HPEN hPen;

		hBrush = CreateSolidBrush(RGB(255, 0, 0));
		oldBrush = SelectObject(hdc, hBrush);

		hPen = CreatePen(PS_NULL, 0, RGB(255, 0, 0));
		oldPen = SelectObject(hdc, hPen);

		SetBkMode(hdc, TRANSPARENT);

		if (g_posType == TRANSPARENT_WINDOW_TOP_LEFT || g_posType == TRANSPARENT_WINDOW_BOTTOM_LEFT)
		{
			m_rectRC2Temp1;
			m_rectRC2Temp1.top = 0;
			m_rectRC2Temp1.left = 0;
			m_rectRC2Temp1.right = 260;
			m_rectRC2Temp1.bottom = 20;

			//DrawText(hdc, "��", lstrlen("��"), &m_rectRC2Temp1, DT_SINGLELINE | DT_LEFT | DT_VCENTER);
		}
		else if (g_posType == TRANSPARENT_WINDOW_TOP || g_posType == TRANSPARENT_WINDOW_BOTTOM)
		{
			m_rectRC2Temp1;
			m_rectRC2Temp1.top = 0;
			m_rectRC2Temp1.left = 0;
			m_rectRC2Temp1.right = 260;
			m_rectRC2Temp1.bottom = 20;

			//DrawText(hdc, "��", lstrlen("��"), &m_rectRC2Temp1, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

		}
		else if (g_posType == TRANSPARENT_WINDOW_TOP_RIGHT || g_posType == TRANSPARENT_WINDOW_BOTTOM_RIGHT)
		{
			//m_rectRC2Temp1;
			//m_rectRC2Temp1.top = 0;
			//m_rectRC2Temp1.left = 0;
			//m_rectRC2Temp1.right = 260;
			//m_rectRC2Temp1.bottom = 20;

			//DrawText(hdc, "��", lstrlen("��"), &m_rectRC2Temp1, DT_SINGLELINE | DT_RIGHT | DT_VCENTER);
			Ellipse(hdc, 255, 25, 245, 15);
		}

		//SelectObject(hdc, oldfont);
		SelectObject(hdc, oldPen);
		SelectObject(hdc, oldBrush);

		DeleteObject(oldBrush);
		DeleteObject(oldPen);

		ReleaseDC(hwnd, hdc);

		EndPaint(hwnd, &ps);
	}
	return 0;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}