#include "IGuiProcess.h"

void RunGuiMeshAgent()
{
	char targetexe[_MAX_PATH + 40];
	size_t targetexelen = 0;
		
	if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targetexe) != S_FALSE)
	{
		targetexelen = strnlen_s(targetexe, _MAX_PATH + 40);
		if (targetexelen <= MAX_PATH) memcpy_s(targetexe + targetexelen, _MAX_PATH + 40 - targetexelen, "\\TRA Agent\\TRAAgent.exe -gui", 38);
		//targetexelen += 25;
	}

	RunAsUserSelf(targetexe, TRUE);
}

void RunConsoleMeshAgent()
{
	char targetexe[_MAX_PATH + 40];
	size_t targetexelen = 0;

	if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targetexe) != S_FALSE)
	{
		targetexelen = strnlen_s(targetexe, _MAX_PATH + 40);
		if (targetexelen <= MAX_PATH) memcpy_s(targetexe + targetexelen, _MAX_PATH + 40 - targetexelen, "\\TRA Agent\\TRAAgent.exe", 38);
		//targetexelen += 25;
	}

	RunAsUserSelf(targetexe, TRUE);
}

void RunAllowRemoteMeshAgent(int socketNumber)
{
	char targetexe[_MAX_PATH + 40];
	char socketStr[10];
	size_t targetexelen = 0;

	if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targetexe) != S_FALSE)
	{
		targetexelen = strnlen_s(targetexe, _MAX_PATH + 40);
		if (targetexelen <= MAX_PATH)
		{
			memcpy_s(targetexe + targetexelen, _MAX_PATH + 40 - targetexelen, "\\TRA Agent\\TRAAgent.exe -allowremote", 48);
		}
		//targetexelen += 25;

		// 20190402 Allow Remote
		sprintf(socketStr, " %d", socketNumber);
		strcat(targetexe, socketStr);
	}

	RunAsUserSelf(targetexe, TRUE);
}

void RunSupervisor() 
{
	char targetexe[_MAX_PATH + 40];
	size_t targetexelen = 0;

	if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targetexe) != S_FALSE)
	{
		targetexelen = strnlen_s(targetexe, _MAX_PATH + 40);
		if (targetexelen <= MAX_PATH) memcpy_s(targetexe + targetexelen, _MAX_PATH + 40 - targetexelen, "\\TRA Agent\\supervisor.exe", 38);
		//targetexelen += 25;
	}

	// Service param TRUE
	// GUI param FALSE
	RunAsUserSelf(targetexe, TRUE);
}

void RunSender()
{
	char targetexe[_MAX_PATH + 40];
	size_t targetexelen = 0;

	if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targetexe) != S_FALSE)
	{
		targetexelen = strnlen_s(targetexe, _MAX_PATH + 40);
		if (targetexelen <= MAX_PATH) memcpy_s(targetexe + targetexelen, _MAX_PATH + 40 - targetexelen, "\\TRA Agent\\sender.exe", 38);
		//targetexelen += 25;
	}

	// Service param TRUE
	// GUI param FALSE
	RunAsUserSelf(targetexe, TRUE);
}

void RunUpdater()
{
	char targetexe[_MAX_PATH + 40];
	size_t targetexelen = 0;

	if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targetexe) != S_FALSE)
	{
		targetexelen = strnlen_s(targetexe, _MAX_PATH + 40);
		if (targetexelen <= MAX_PATH) memcpy_s(targetexe + targetexelen, _MAX_PATH + 40 - targetexelen, "\\TRA Agent\\updater.exe", 38);
		//targetexelen += 25;
	}

	// Service param TRUE
	// GUI param FALSE
	RunAsUserSelf(targetexe, TRUE);
}

BOOL RunAsUserSelf(char* cmd, BOOL isWinlogon)
{
	PROCESS_INFORMATION pi;
	STARTUPINFO sti;
	HANDLE userToken;

	LPVOID pEnv = NULL;
	DWORD dwCreationFlags = NORMAL_PRIORITY_CLASS | CREATE_NEW_CONSOLE;

	ZeroMemory(&sti, sizeof(STARTUPINFO));
	sti.cb = sizeof(STARTUPINFO);
	sti.lpDesktop = "winsta0\\default";

	userToken = GetToken(isWinlogon);

	if (!userToken)
		return FALSE;

	if (CreateEnvironmentBlock(&pEnv, userToken, TRUE))
	{
		dwCreationFlags |= CREATE_UNICODE_ENVIRONMENT;
	}
	else
	{
		pEnv = NULL;
	}

	if (ImpersonateLoggedOnUser(userToken))
	{

		if (CreateProcessAsUser(
			userToken,            
			NULL,              
			cmd,
			NULL,              // pointer to process SECURITY_ATTRIBUTES
			NULL,              // pointer to thread SECURITY_ATTRIBUTES
			FALSE,             // handles are not inheritable
			dwCreationFlags,   // creation flags
			pEnv,              // pointer to new environment block 
			NULL,              // name of current directory 
			&sti,              // pointer to STARTUPINFO structure
			&pi                // receives information about new process
		) == 0)
		{
			return FALSE;
		}
	}

	RevertToSelf();

	CloseHandle(userToken);

	return TRUE;
}

HANDLE GetToken(BOOL isWinlogon)
{
	HANDLE hProcess = NULL, hPToken = NULL;
	DWORD dwSessionId = 0, dwExplorerLogonPid = 0;

	// 세션 ID 얻기
	typedef DWORD(WINAPI* pWTSGetActiveConsoleSessionId)(VOID);
	typedef BOOL(WINAPI* pProcessIdToSessionId)(DWORD, DWORD*);

	pWTSGetActiveConsoleSessionId WTSGetActiveConsoleSessionIdF = NULL;
	pProcessIdToSessionId pProcessIdToSessionIdF = NULL;

	HMODULE hlibkernel = LoadLibrary("kernel32.dll");
	WTSGetActiveConsoleSessionIdF = (pWTSGetActiveConsoleSessionId)GetProcAddress(hlibkernel, "WTSGetActiveConsoleSessionId");
	pProcessIdToSessionIdF = (pProcessIdToSessionId)GetProcAddress(hlibkernel, "ProcessIdToSessionId");
	FreeLibrary(hlibkernel);

	if (WTSGetActiveConsoleSessionIdF == NULL || pProcessIdToSessionIdF == NULL) return NULL;

	DWORD session_count = 0;
	WTS_SESSION_INFOW *pSession = NULL;
	if (WTSEnumerateSessions(WTS_CURRENT_SERVER_HANDLE, 0, 1, &pSession, &session_count))
	{

	}
	for (int i = 0; i < session_count; i++)
	{
		DWORD session_id = pSession[i].SessionId;
		WTS_CONNECTSTATE_CLASS wts_connect_state = WTSDisconnected;
		WTS_CONNECTSTATE_CLASS* ptr_wts_connect_state = NULL;

		DWORD bytes_returned = 0;
		if (WTSQuerySessionInformation(
			WTS_CURRENT_SERVER_HANDLE,
			session_id,
			WTSConnectState,
			&ptr_wts_connect_state,
			&bytes_returned))
		{
			wts_connect_state = *ptr_wts_connect_state;
			WTSFreeMemory(ptr_wts_connect_state);
			if (wts_connect_state != WTSActive) continue;
		}
		else
		{
			continue;
		}

		dwSessionId = session_id;
	}

	//if (AnySupportTools::isWinVistaOrHigher() && dwSessionId == 0) // 로그오프 등
	if (dwSessionId == 0)
	{
		return FALSE;
	}

	PROCESSENTRY32 procEntry;
	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnap == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}
	procEntry.dwSize = sizeof(PROCESSENTRY32);
	if (!Process32First(hSnap, &procEntry))
	{
		return FALSE;
	}

	do
	{
		if (isWinlogon)
		{
			if (strcmp(procEntry.szExeFile, "winlogon.exe") == 0)
			{
				DWORD dwExplorerSessId = 0;
				if (pProcessIdToSessionIdF(procEntry.th32ProcessID, &dwExplorerSessId))
				{

				}

				if (pProcessIdToSessionIdF(procEntry.th32ProcessID, &dwExplorerSessId) && dwExplorerSessId == dwSessionId)
				{
					dwExplorerLogonPid = procEntry.th32ProcessID;
					break;
				}
			}
		}
		else
		{
			if (strcmp(procEntry.szExeFile, "explorer.exe") == 0)
			{
				DWORD dwExplorerSessId = 0;
				if (pProcessIdToSessionIdF(procEntry.th32ProcessID, &dwExplorerSessId))
				{

				}

				if (pProcessIdToSessionIdF(procEntry.th32ProcessID, &dwExplorerSessId) && dwExplorerSessId == dwSessionId)
				{
					dwExplorerLogonPid = procEntry.th32ProcessID;
					break;
				}
			}
		}

	} while (Process32Next(hSnap, &procEntry));
	CloseHandle(hSnap);


	hProcess = OpenProcess(MAXIMUM_ALLOWED, FALSE, dwExplorerLogonPid);
	if (!hProcess)
	{

	}

	if (!OpenProcessToken(hProcess, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY
		| TOKEN_DUPLICATE | TOKEN_ASSIGN_PRIMARY | TOKEN_ADJUST_SESSIONID
		| TOKEN_READ | TOKEN_WRITE, &hPToken))
	{
		printf("Process token open Error: %u\n", GetLastError());
	}
	CloseHandle(hProcess);

	return hPToken;
}

BOOL SearchProcessByName(char* processname)
{
	DWORD dwSize = 250;
	HANDLE hSnapShot;
	PROCESSENTRY32 pEntry;
	BOOL bCrrent = FALSE;

	hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);

	pEntry.dwSize = sizeof(pEntry);

	Process32First(hSnapShot, &pEntry);

	while (1)
	{
		BOOL hRes = Process32Next(hSnapShot, &pEntry);

		if (hRes == FALSE)
			break;

		//if (!wcsncmp(pEntry.szExeFile, processname, 15))
		if (strcmp(pEntry.szExeFile, processname) == 0)
		{
			CloseHandle(hSnapShot);
			return TRUE;
		}
	}

	CloseHandle(hSnapShot);
	return FALSE;
}

HANDLE GetProcessHandleByName(char* szFilename)
{
	HANDLE hProcessSnapshot;
	HANDLE hProcess;
	PROCESSENTRY32 pe32;
	DWORD sessionID;
	DWORD sessionIDTemp;

	sessionID = WTSGetActiveConsoleSessionId();
	hProcessSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);

	if (hProcessSnapshot == INVALID_HANDLE_VALUE)
		return INVALID_HANDLE_VALUE;

	pe32.dwSize = sizeof(PROCESSENTRY32);

	Process32First(hProcessSnapshot, &pe32);

	do
	{
		hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
		if (hProcess != NULL)
		{
			//if (_tcscmp(pe32.szExeFile, szFilename) == 0)
			if (strcmp(pe32.szExeFile, szFilename) == 0)
			{
				if (ProcessIdToSessionId(pe32.th32ProcessID, &sessionIDTemp) && sessionIDTemp == sessionID)
				{
					//wprintf(L"Process ID of %s : %d\n", pe32.szExeFile, pe32.th32ProcessID);
					CloseHandle(hProcessSnapshot);
					return hProcess;
				}
			}
		}
		CloseHandle(hProcess);
	} while (Process32Next(hProcessSnapshot, &pe32));

	CloseHandle(hProcessSnapshot);
	return INVALID_HANDLE_VALUE;
}

BOOL TerminateProcessByName(char* szFilename)
{
	BOOL bRet = FALSE;
	HANDLE hProcess = GetProcessHandleByName(szFilename);

	if (hProcess != INVALID_HANDLE_VALUE)
	{
		if (TerminateProcess(hProcess, 0))
			bRet = TRUE;
	}

	CloseHandle(hProcess);
	return bRet;
}

BOOL TerminateSlaveProcess()
{
	HANDLE hProcessSnapshot;
	HANDLE hProcess;
	PROCESSENTRY32 pe32;
	DWORD sessionID;
	DWORD sessionIDTemp;
	BOOL bRet = FALSE;

	sessionID = WTSGetActiveConsoleSessionId();
	hProcessSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);

	if (hProcessSnapshot == INVALID_HANDLE_VALUE)
		return INVALID_HANDLE_VALUE;

	pe32.dwSize = sizeof(PROCESSENTRY32);

	Process32First(hProcessSnapshot, &pe32);

	do
	{
		hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
		if (hProcess != NULL)
		{
			//if (_tcscmp(pe32.szExeFile, "TRAAgent.exe") == 0)
			if (strcmp(pe32.szExeFile, "TRAAgent.exe") == 0)
			{
				if (ProcessIdToSessionId(pe32.th32ProcessID, &sessionIDTemp) && sessionIDTemp == sessionID)
				{
					//wprintf(L"Process ID of %s : %d\n", pe32.szExeFile, pe32.th32ProcessID);

					//if (IsSlaveProcess(pe32.th32ProcessID))
					{
						HANDLE slaveProc = GetProcessHandleByPID(pe32.th32ProcessID);

						if (slaveProc != INVALID_HANDLE_VALUE)
						{
							if (TerminateProcess(slaveProc, 0)) {

								bRet = TRUE;
							}
						}

						CloseHandle(slaveProc);
						CloseHandle(hProcess);
						CloseHandle(hProcessSnapshot);
						//return bRet;
					}

					CloseHandle(hProcessSnapshot);
				}
			}
		}
		CloseHandle(hProcess);
	} while (Process32Next(hProcessSnapshot, &pe32));

	CloseHandle(hProcessSnapshot);

	return bRet;
}

HANDLE GetProcessHandleByPID(int pid)
{
	HANDLE hProcess;
	DWORD sessionID;
	DWORD sessionIDTemp;

	sessionID = WTSGetActiveConsoleSessionId();

	hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if (hProcess != NULL)
	{
		return hProcess;
	}

	CloseHandle(hProcess);
	return INVALID_HANDLE_VALUE;
}

PVOID GetPebAddress(HANDLE ProcessHandle)
{
	HMODULE hNtDll = LoadLibrary("ntdll.dll");
	if (hNtDll == NULL) return NULL;

	_NtQueryInformationProcess NtQueryInformationProcess =
		(_NtQueryInformationProcess)GetProcAddress(
			hNtDll, "NtQueryInformationProcess");
	PROCESS_BASIC_INFORMATION pbi;

	NtQueryInformationProcess(ProcessHandle, 0, &pbi, sizeof(pbi), NULL);

	return pbi.PebBaseAddress;
}

BOOL IsSlaveProcess(int pid)
{
	HANDLE processHandle;
	PVOID pebAddress;
	PVOID rtlUserProcParamsAddress;
	UNICODE_STRING commandLine;
	//WCHAR *commandLineContents;
	char *commandLineContents;

	if ((processHandle = OpenProcess(
		PROCESS_QUERY_INFORMATION | 
		PROCESS_VM_READ,
		FALSE, pid)) == 0)
	{

		return FALSE;
	}

	pebAddress = GetPebAddress(processHandle);

	if (!ReadProcessMemory(processHandle, (PCHAR)pebAddress + 0x10,
		&rtlUserProcParamsAddress, sizeof(PVOID), NULL))
	{
		return FALSE;
	}

	if (!ReadProcessMemory(processHandle, (PCHAR)rtlUserProcParamsAddress + 0x40,
		&commandLine, sizeof(commandLine), NULL))
	{
		return FALSE;
	}

	commandLineContents = (char *)malloc(commandLine.Length);

	if (!ReadProcessMemory(processHandle, commandLine.Buffer,
		commandLineContents, commandLine.Length, NULL))
	{
		return FALSE;
	}

	//if (wcsstr(commandLineContents, "-gui"))
	if (strstr(commandLineContents, "-gui") != NULL)
	{
		CloseHandle(processHandle);
		free(commandLineContents);
		return TRUE;
	}

	CloseHandle(processHandle);
	free(commandLineContents);
	return FALSE;
}