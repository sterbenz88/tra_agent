#include <WTypes.h>

typedef struct _ScreenInfo
{
	int bGetTaskBarPos;
	int bGetDualMonitorInfo;
	int nMonitorCount;
	unsigned int nDesktopHeight;
	unsigned int nDesktopWidth;
	unsigned int nScreenWidth;
	unsigned int nScreenHeight;
	unsigned int nDualScreenWidth;
	RECT rcDesktop;
	int nTaskBarPos;
	DEVMODE devMode[5];

	int	nDevideLinePos_x;
	int	nTextPos_x;
}_ScreenInfo;

void red_test(int i);

void get_system_info();
void set_window_rect(int posType);
void move_rc1_wnd(int x, int y);
void move_rc2_wnd(int x, int y);

void create_rc_wnds();
void create_rc1_wnd();
void create_rc2_wnd();
void destroy_rc_wnds();

void refresh_rc_wnds(int posType);
void show_rc_wnds(int posType);
void hide_rc_wnds();

static LRESULT CALLBACK wnd_rc1(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
static LRESULT CALLBACK wnd_rc2(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);