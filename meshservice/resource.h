//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// MeshService.rc에서 사용되고 있습니다.
//
#define IDI_ICON1                       101
#define IDD_INSTALLDIALOG               103
#define IDD_GUI_DEFAULT                 104
#define IDC_SYSTRAYDEMO					105
#define IDD_ALLOW_REMOTE                106
#define IDM_EXIT						107
#define IDC_BUTTON2                     1002
#define IDC_INSTALLBUTTON               1002
#define IDC_BUTTON3                     1004
#define IDC_UNINSTALLBUTTON             1004
#define IDC_STATUSTEXT                  1005
#define IDC_VERSIONTEXT                 1006
#define IDC_ALLOW_REMOTE_QU             1006
#define IDC_HASHTEXT                    1007
#define IDC_HASHTEXT2                   1008
#define IDC_POLICYTEXT                  1008
#define IDC_UNINSTALLBUTTON2            1009
#define IDC_CONNECTBUTTON               1009
#define IDC_HASHTEXT3                   1011
#define IDC_SERVERID                    1011
#define IDC_SERVERLOCATION              1012
#define IDDISCONNECT                    1013
#define IDC_SESSION_LIST                1014

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
