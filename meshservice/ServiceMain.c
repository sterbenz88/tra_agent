/*
Copyright 2006 - 2018 Intel Corporation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#if defined(WINSOCK2)
	#include <winsock2.h>
	#include <ws2tcpip.h>
#elif defined(WINSOCK1)
	#include <winsock.h>
	#include <wininet.h>
#endif

#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <winhttp.h>
#include <shlobj.h>
#include "resource.h"
#include "meshcore/signcheck.h"
#include "meshcore/meshdefines.h"
#include "meshcore/meshinfo.h"
#include "microstack/ILibParsers.h"
#include "microstack/ILibCrypto.h"
#include "meshcore/agentcore.h"
#include "microscript/ILibDuktape_ScriptContainer.h"
#include "microscript/ILibDuktape_Commit.h"
#include <shellscalingapi.h>

#include "meshcore/TRA/ITraCore.h"
// pjh 20190328 Red Dot (Run mode)
#include "meshcore/GUI/IRedDotWnd.h"
#include "meshcore/GUI/IGuiProcess.h"

#include "meshcore/TRA/ITraUdpProc.h"
#include "meshcore/TRA/ITraSessionList.h"

#include "microstack/ILibWebRTC.h"
#include "microstack/ILibWrapperWebRTC.h"

#include "meshcore/TRA/ITraTimer.h"
#include "meshcore/TRA/ITraUtil.h"
#include "meshcore/TRA/ITraFileLog.h"

#ifndef _MINCORE
// #include "../kvm/kvm.h"
int SetupWindowsFirewall(wchar_t* processname);
int ClearWindowsFirewall(wchar_t* processname);
#endif

#if defined(WIN32) && defined (_DEBUG) && !defined(_MINCORE)
#include <crtdbg.h>
#define _CRTDBG_MAP_ALLOC
#endif

#include <WtsApi32.h>

#ifdef _LINKVM
	#ifdef WIN32
		#include "meshcore/KVM/Windows/kvm.h"
	#endif	
	#ifdef _POSIX
		#ifndef __APPLE__
			#include "meshcore/KVM/Linux/linux_kvm.h"
		#else
			#include "meshcore/KVM/MacOS/mac_kvm.h"
		#endif
	#endif
#endif

TCHAR* serviceFile = TEXT("TRA Agent");
TCHAR* serviceFileOld = TEXT("TRA Agent v2");
TCHAR* serviceName = TEXT("TRA Agent background service");
TCHAR* serviceDesc = TEXT("Remote monitoring and management service.");

SERVICE_STATUS serviceStatus;
SERVICE_STATUS_HANDLE serviceStatusHandle = 0;
INT_PTR CALLBACK DialogHandler(HWND, UINT, WPARAM, LPARAM);

MeshAgentHostContainer *agent = NULL;
DWORD g_serviceArgc;
char **g_serviceArgv;

// pjh 20190328 Red Dot (Run mode)
INT_PTR CALLBACK GuiDialogHandler(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK AllowRemoteDialogHandler(HWND, UINT, WPARAM, LPARAM);
DWORD WINAPI StartTempAgent(_In_ LPVOID lpParameter);

// pjh 20190420 GuiRecv
DWORD threadConsoleID;
HANDLE m_ThreadConsole;
LRESULT CALLBACK DoUdpConsoleRecv(LPVOID lpParameter);
HWND hwndList;

DWORD threadGuiID;
HANDLE m_ThreadGui;
LRESULT CALLBACK DoUdpGuiRecv(LPVOID lpParameter);

DWORD threadSessionID;
HANDLE m_ThreadSession;
LRESULT CALLBACK DoThreadSession(LPVOID lpParameter);

// KVM
DWORD threadKVMID;
HANDLE m_ThreadKVM;
LRESULT CALLBACK DoKVMRecv(LPVOID lpParameter);

extern HINSTANCE g_hres;
extern int g_clientCount;
extern BOOL g_serviceMode;
extern int g_socketNumber;

extern BOOL g_testMode;
extern char g_testAgentName[255];

extern void ILibDuktape_ScriptContainer_FORCE_DISCONNECT(int socketNumber);

extern int g_kvm_type;
extern BOOL g_isCallForceDisconnect;

extern BOOL g_timer_shutdown;
extern BOOL g_timer_use;
extern BOOL g_timer_session_change;
extern BOOL g_me_version;

//extern ILibWrapper_WebRTC_DataChannel *g_DataChannel;

/*
extern int g_TrustedHashSet;
extern char g_TrustedHash[32];
extern char NullNodeId[32];
extern struct PolicyInfoBlock* g_TrustedPolicy;
extern char g_selfid[UTIL_HASHSIZE];
extern struct sockaddr_in6 g_ServiceProxy;
extern char* g_ServiceProxyHost;
extern int g_ServiceConnectFlags;
*/

#if defined(_LINKVM)
extern DWORD WINAPI kvm_server_mainloop(LPVOID Param);
#endif

BOOL IsAdmin()
{
	BOOL admin;
	PSID AdministratorsGroup; 
	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;

	if ((admin = AllocateAndInitializeSid( &NtAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, &AdministratorsGroup)) != 0)
	{
		if (!CheckTokenMembership( NULL, AdministratorsGroup, &admin)) admin = FALSE;
		FreeSid(AdministratorsGroup);
	}
	return admin;
}

BOOL RunAsAdmin(char* args) {
	char szPath[_MAX_PATH + 100];
	if (GetModuleFileNameA(NULL, szPath, _MAX_PATH))
	{
		SHELLEXECUTEINFO sei = { sizeof(sei) };
		sei.hwnd = NULL;
		sei.nShow = SW_NORMAL;
		sei.lpVerb = "runas";
		sei.lpFile = szPath;
		sei.lpParameters = args;
		return ShellExecuteExA(&sei);
	}
	return FALSE;
}

DWORD WINAPI ServiceControlHandler( DWORD controlCode, DWORD eventType, void *eventData, void* eventContext )
{
	switch (controlCode)
	{
		case SERVICE_CONTROL_INTERROGATE:
			break;
		case SERVICE_CONTROL_SHUTDOWN:
		case SERVICE_CONTROL_STOP:
			serviceStatus.dwCurrentState = SERVICE_STOP_PENDING;
			SetServiceStatus( serviceStatusHandle, &serviceStatus );
			if (agent != NULL) { MeshAgent_Stop(agent); }
			return(0);
		case SERVICE_CONTROL_POWEREVENT:
			switch (eventType)
			{
				case PBT_APMPOWERSTATUSCHANGE:	// Power status has changed.
					break;
				case PBT_APMRESUMEAUTOMATIC:	// Operation is resuming automatically from a low - power state.This message is sent every time the system resumes.
					break;
				case PBT_APMRESUMESUSPEND:		// Operation is resuming from a low - power state.This message is sent after PBT_APMRESUMEAUTOMATIC if the resume is triggered by user input, such as pressing a key.
					break;
				case PBT_APMSUSPEND:			// System is suspending operation.
					break;
				case PBT_POWERSETTINGCHANGE:	// Power setting change event has been received.
					break;
			}
			break;
		case SERVICE_CONTROL_SESSIONCHANGE:
			if (agent == NULL)
			{
				break; // If there isn't an agent, no point in doing anything, cuz nobody will hear us
			}

			switch (eventType)
			{
				case WTS_CONSOLE_CONNECT:		// The session identified by lParam was connected to the console terminal or RemoteFX session.
					break;
				case WTS_CONSOLE_DISCONNECT:	// The session identified by lParam was disconnected from the console terminal or RemoteFX session.
					break;
				case WTS_REMOTE_CONNECT:		// The session identified by lParam was connected to the remote terminal.
					break;
				case WTS_REMOTE_DISCONNECT:		// The session identified by lParam was disconnected from the remote terminal.
					break;
				case WTS_SESSION_LOGON:			// A user has logged on to the session identified by lParam.
					// 20190629 - pjh
					if (g_serviceMode)
					{
						g_timer_session_change = TRUE;

						//RunGuiMeshAgent();

						// 20190424 pjh
						m_ThreadSession = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(DoThreadSession), (LPVOID)NULL, 0, &threadSessionID);
						ResumeThread(m_ThreadSession);
					}
					break;
				case WTS_SESSION_LOGOFF:		// A user has logged off the session identified by lParam.

					// 20190729 - kvm process reset.
					if (g_serviceMode)
					{
						if (g_isCallForceDisconnect)
						{
							CoUninitialize();
							exit(0);
						}
					}
					break;
				case WTS_SESSION_LOCK:			// The session identified by lParam has been locked.
					break;
				case WTS_SESSION_UNLOCK:		// The session identified by lParam has been unlocked.
					break;
				case WTS_SESSION_REMOTE_CONTROL:// The session identified by lParam has changed its remote controlled status.To determine the status, call GetSystemMetrics and check the SM_REMOTECONTROL metric.
					break;
				case WTS_SESSION_CREATE:		// Reserved for future use.
				case WTS_SESSION_TERMINATE:		// Reserved for future use.
					break;
			}
			break;
		default:
			break;
	}

	SetServiceStatus( serviceStatusHandle, &serviceStatus );
	return(0);
}


// Add the uninstallation icon in the Windows Control Panel.
void WINAPI AddUninstallIcon()
{
	/*
	[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\TRAAgent]
	"DisplayName"="TRA Agent Service - Remote Control Software"
	"Publisher"="Thingscare"
	"MajorVersion"="2"
	"MinorVersion"="13"
	"InstallLocation"="C:\\Program Files\\TRA Agent"
	"UninstallString"="C:\\Program Files\\TRA Agent\\meshuninstaller.bat"
	"DisplayIcon"="C:\\Program Files\\TRA Agent\\TRAAgent.exe"
	"DisplayVersion"="2.1.3"
	"URLInfoAbout"="http://tra.thingscare.com/"
	"VersionMajor"=dword:00000002
	"VersionMinor"=dword:00000013
	"EstimatedSize"=dword:00208000
	"NoModify"=dword:00000001
	"NoRepair"=dword:00000001
	*/

	if (g_me_version)
		return;

	int i;
	HKEY hKey;
	if (RegCreateKey(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\TRAAgent", &hKey) == ERROR_SUCCESS)
	{
		LPCTSTR str;
		char targetexe[_MAX_PATH + 40];
		size_t targetexelen = 0;

		str = "TRA Agent - Remote Control Software\0";
		RegSetValueEx(hKey, "DisplayName", 0, REG_SZ, (LPBYTE)str, (DWORD)strlen(str)+1);

		str = "Thingscare\0";
		RegSetValueEx(hKey, "Publisher", 0, REG_SZ, (LPBYTE)str, (DWORD)strlen(str) + 1);

		str = "1\0";
		RegSetValueEx(hKey, "MajorVersion", 0, REG_SZ, (LPBYTE)str, (DWORD)strlen(str) + 1);

		str = "0\0";
		RegSetValueEx(hKey, "MinorVersion", 0, REG_SZ, (LPBYTE)str, (DWORD)strlen(str) + 1);

		// Install location
		if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targetexe) != S_FALSE) {
			targetexelen = strnlen_s(targetexe, _MAX_PATH + 40);
			if (targetexelen <= MAX_PATH) {
				memcpy_s(targetexe + targetexelen, _MAX_PATH + 40 - targetexelen, "\\TRA Agent\\\0", 13);
				RegSetValueEx(hKey, "InstallLocation", 0, REG_SZ, (LPBYTE)targetexe, (DWORD)strlen(targetexe) + 1);
			}
		}

		// Uninstall command
		if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targetexe) != S_FALSE) {
			targetexelen = strnlen_s(targetexe, _MAX_PATH + 40);
			if (targetexelen <= MAX_PATH) {
				memcpy_s(targetexe + targetexelen, _MAX_PATH + 40 - targetexelen, "\\TRA Agent\\TRAAgent.exe -fulluninstall\0", 41);
				RegSetValueEx(hKey, "UninstallString", 0, REG_SZ, (LPBYTE)targetexe, (DWORD)strlen(targetexe) + 1);
			}
		}

		// Display icon
		if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targetexe) != S_FALSE) {
			targetexelen = strnlen_s(targetexe, _MAX_PATH + 40);
			if (targetexelen <= MAX_PATH) {
				memcpy_s(targetexe + targetexelen, _MAX_PATH + 40 - targetexelen, "\\TRA Agent\\TRAAgent.exe\0", 26);
				RegSetValueEx(hKey, "DisplayIcon", 0, REG_SZ, (LPBYTE)targetexe, (DWORD)strlen(targetexe) + 1);
			}
		}

		str = "1.0.0\0";
		RegSetValueEx(hKey, "DisplayVersion", 0, REG_SZ, (LPBYTE)str, (DWORD)strlen(str) + 1);

		str = "http://tra.thingscare.com/\0"; // TODO - Change this to .msg content
		RegSetValueEx(hKey, "URLInfoAbout", 0, REG_SZ, (LPBYTE)str, (DWORD)strlen(str) + 1);

		i = 2;
		RegSetValueEx(hKey, "VersionMajor", 0, REG_DWORD, (BYTE*)&i, (DWORD)4);

		i = 13;
		RegSetValueEx(hKey, "VersionMinor", 0, REG_DWORD, (BYTE*)&i, (DWORD)4);

		//i = 0x00208000;
		// pjh 20190504
		i = 0x00000c00;
		RegSetValueEx(hKey, "EstimatedSize", 0, REG_DWORD, (BYTE*)&i, (DWORD)4);

		i = 1;
		RegSetValueEx(hKey, "NoModify", 0, REG_DWORD, (BYTE*)&i, (DWORD)4);
		RegSetValueEx(hKey, "NoRepair", 0, REG_DWORD, (BYTE*)&i, (DWORD)4);

		RegCloseKey(hKey);
	}
	else
	{
		printf("Error writing to registry, try running as administrator.");
	}

}


void WINAPI RemoveUninstallIcon()
{
	if (g_me_version)
		return;

	RegDeleteKey(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\TRAAgent");
}


void WINAPI ServiceMain(DWORD argc, LPTSTR *argv)
{
	ILib_DumpEnabledContext winException;
	size_t len = 0;
	WCHAR str[_MAX_PATH];


	UNREFERENCED_PARAMETER( argc );
	UNREFERENCED_PARAMETER( argv );

	// Initialise service status
	serviceStatus.dwServiceType = SERVICE_WIN32;
	serviceStatus.dwCurrentState = SERVICE_STOPPED;
	serviceStatus.dwControlsAccepted = 0;
	serviceStatus.dwWin32ExitCode = NO_ERROR;
	serviceStatus.dwServiceSpecificExitCode = NO_ERROR;
	serviceStatus.dwCheckPoint = 0;
	serviceStatus.dwWaitHint = 0;
	serviceStatusHandle = RegisterServiceCtrlHandlerExA(serviceName, (LPHANDLER_FUNCTION_EX)ServiceControlHandler, NULL);

	if (serviceStatusHandle)
	{
		// Service is starting
		serviceStatus.dwCurrentState = SERVICE_START_PENDING;
		SetServiceStatus(serviceStatusHandle, &serviceStatus);

		// Service running
		serviceStatus.dwControlsAccepted |= (SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_POWEREVENT | SERVICE_ACCEPT_SESSIONCHANGE);
		serviceStatus.dwCurrentState = SERVICE_RUNNING;
		SetServiceStatus( serviceStatusHandle, &serviceStatus);

		// Get our own executable name
		GetModuleFileNameW(NULL, str, _MAX_PATH);

#ifndef _MINCORE
		// Setup firewall
		SetupWindowsFirewall(str);
#endif

		// Run the mesh agent
		CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

		__try
		{
			agent = MeshAgent_Create(0);
			MeshAgent_Start(agent, g_serviceArgc, g_serviceArgv);
			agent = NULL;
		}
		__except (ILib_WindowsExceptionFilterEx(GetExceptionCode(), GetExceptionInformation(), &winException))
		{
			ILib_WindowsExceptionDebugEx(&winException);
		}
		CoUninitialize();

		// Service was stopped
		serviceStatus.dwCurrentState = SERVICE_STOP_PENDING;
		SetServiceStatus(serviceStatusHandle, &serviceStatus);

		// Service is now stopped
		serviceStatus.dwControlsAccepted &= ~(SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN);
		serviceStatus.dwCurrentState = SERVICE_STOPPED;
		SetServiceStatus(serviceStatusHandle, &serviceStatus);
	}
}

int RunService(int argc, char* argv[])
{
	SERVICE_TABLE_ENTRY serviceTable[2];
	serviceTable[0].lpServiceName = serviceName;
	serviceTable[0].lpServiceProc = (LPSERVICE_MAIN_FUNCTION)ServiceMain;
	serviceTable[1].lpServiceName = NULL;
	serviceTable[1].lpServiceProc = NULL;
	g_serviceArgc = argc;
	g_serviceArgv = argv;

	return StartServiceCtrlDispatcher( serviceTable );
}

BOOL InstallService()
{
	SC_HANDLE serviceControlManager = OpenSCManager( 0, 0, SC_MANAGER_CREATE_SERVICE );
	SERVICE_DESCRIPTION sd;
	SERVICE_DELAYED_AUTO_START_INFO as;
	SERVICE_FAILURE_ACTIONS fa;
	SC_ACTION failactions[3];
	BOOL r = FALSE;

	if ( serviceControlManager )
	{
		char path[1024];
		if (GetModuleFileName( 0, (LPTSTR)path, 1024) > 0)
		{
			// Install the service
			SC_HANDLE service = CreateService( 
				serviceControlManager,
				serviceFile,
				serviceName,
				SERVICE_ALL_ACCESS,
				SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS,
				SERVICE_AUTO_START,
				SERVICE_ERROR_IGNORE,
				(LPCTSTR)path,
				0, 0, 0, 0, 0 );

			if (service)
			{
				// Update the service description
				sd.lpDescription = serviceDesc;
				ChangeServiceConfig2(service, SERVICE_CONFIG_DESCRIPTION, &sd);

				// Update the service auto-start
				as.fDelayedAutostart = FALSE;
				ChangeServiceConfig2(service, SERVICE_CONFIG_DELAYED_AUTO_START_INFO, &as);

				// Update the faliure action
				// 20190727 - pjh service restart time
				failactions[0].Type = SC_ACTION_RESTART;
				failactions[0].Delay = 5000;                          // Wait 1 minutes before faliure restart (milliseconds)
				failactions[1].Type = SC_ACTION_RESTART;
				failactions[1].Delay = 5000;                          // Wait 1 minutes before faliure restart (milliseconds)
				failactions[2].Type = SC_ACTION_RESTART;
				failactions[2].Delay = 5000;
				/*
				failactions[0].Type = SC_ACTION_RESTART;
				failactions[0].Delay = 60000;                          // Wait 1 minutes before faliure restart (milliseconds)
				failactions[1].Type = SC_ACTION_RESTART;
				failactions[1].Delay = 60000;                          // Wait 1 minutes before faliure restart (milliseconds)
				failactions[2].Type = SC_ACTION_RESTART;
				failactions[2].Delay = 60000;
				*/
				memset(&fa, 0, sizeof(SERVICE_FAILURE_ACTIONS));
				fa.dwResetPeriod = 86400;					// After 1 days, reset the faliure counters (seconds)
				fa.cActions = 3;
				fa.lpsaActions = failactions;
				r = ChangeServiceConfig2(service, SERVICE_CONFIG_FAILURE_ACTIONS, &fa);

				// Cleanup
				CloseServiceHandle( service );
				#ifdef _DEBUG
				//ILIBMESSAGE("Mesh service installed successfully");
				#endif
			}
			else
			{
				#ifdef _DEBUG
				if(GetLastError() == ERROR_SERVICE_EXISTS)
				{
					ILIBMESSAGE("Mesh service already exists.");
				}
				else
				{
					ILIBMESSAGE("Mesh service was not Installed Successfully.");
				}
				#endif
			}
		}

		CloseServiceHandle( serviceControlManager );
	}
	return r;
}

int UninstallService(TCHAR* serviceName)
{
	int r = 0;
	SC_HANDLE serviceControlManager = OpenSCManager( 0, 0, SC_MANAGER_CONNECT);

	if (serviceControlManager)
	{
		SC_HANDLE service = OpenService( serviceControlManager, serviceName, SERVICE_QUERY_STATUS | DELETE );
		if (service)
		{
			SERVICE_STATUS serviceStatusEx;
			if ( QueryServiceStatus( service, &serviceStatusEx ) )
			{
				if ( serviceStatusEx.dwCurrentState == SERVICE_STOPPED )
				{
					if (DeleteService(service))
					{
						#ifdef _DEBUG
						//ILIBMESSAGE("Mesh service removed successfully");
						#endif
						r = 1;
					}
					else
					{
						#ifdef _DEBUG
						DWORD dwError = GetLastError();
						if(dwError == ERROR_ACCESS_DENIED) {
							ILIBMESSAGE("Access denied while trying to remove mesh service");
						}
						else if(dwError == ERROR_INVALID_HANDLE) {
							ILIBMESSAGE("Handle invalid while trying to remove mesh service");
						}
						else if(dwError == ERROR_SERVICE_MARKED_FOR_DELETE) {
							ILIBMESSAGE("Mesh service already marked for deletion");
						}
						#endif
					}
				}
				else
				{
					r = 2;
					#ifdef _DEBUG
					ILIBMESSAGE("Mesh service is still running");
					#endif
				}
			}
			CloseServiceHandle( service );
		}
		CloseServiceHandle( serviceControlManager );
	}
	return r;
}

// SERVICE_STOPPED				  1    The service is not running.
// SERVICE_START_PENDING		  2    The service is starting.
// SERVICE_STOP_PENDING			  3    The service is stopping.
// SERVICE_RUNNING				  4    The service is running.
// SERVICE_CONTINUE_PENDING		  5    The service continue is pending.
// SERVICE_PAUSE_PENDING		  6    The service pause is pending.
// SERVICE_PAUSED				  7    The service is paused.
// SERVICE_NOT_INSTALLED		100    The service is not installed.
int GetServiceState(LPCSTR servicename)
{
	int r = 0;
	SC_HANDLE serviceControlManager = OpenSCManager(0, 0, SC_MANAGER_CONNECT);

	if (serviceControlManager)
	{
		SC_HANDLE service = OpenService( serviceControlManager, servicename, SERVICE_QUERY_STATUS );
		if (service)
		{
			SERVICE_STATUS serviceStatusEx;
			if ( QueryServiceStatus( service, &serviceStatusEx) )
			{
				r = serviceStatusEx.dwCurrentState;
			}
			CloseServiceHandle( service );
		}
		else
		{
			r = 100;
		}
		CloseServiceHandle( serviceControlManager );
	}
	return r;
}


int LaunchService(LPCSTR servicename)
{
	int r = 0;
	SC_HANDLE serviceControlManager = OpenSCManager(0, 0, SERVICE_QUERY_STATUS | SERVICE_START);

	if (serviceControlManager)
	{
		SC_HANDLE service = OpenService( serviceControlManager, servicename, SERVICE_QUERY_STATUS | SERVICE_START );
		if (service)
		{
			SERVICE_STATUS serviceStatusEx;
			if ( QueryServiceStatus( service, &serviceStatusEx) )
			{
				if (serviceStatusEx.dwCurrentState == SERVICE_STOPPED ) { if (StartService(service, 0, NULL) == TRUE) { r = 1; } } else { r = 2; }
			}
			CloseServiceHandle( service );
		}
		CloseServiceHandle( serviceControlManager );
	}
	return r;
}

int StopService(LPCSTR servicename)
{
	int r = 0;
	SC_HANDLE serviceControlManager = OpenSCManager(0, 0, SERVICE_QUERY_STATUS | SERVICE_STOP);

	if (serviceControlManager)
	{
		SC_HANDLE service = OpenService( serviceControlManager, servicename, SERVICE_QUERY_STATUS | SERVICE_STOP );
		if (service)
		{
			SERVICE_STATUS serviceStatusEx;
			if ( QueryServiceStatus( service, &serviceStatusEx) )
			{
				if (serviceStatusEx.dwCurrentState != SERVICE_STOPPED )
				{
					if (ControlService(service, SERVICE_CONTROL_STOP, &serviceStatusEx) == FALSE)
					{
						// TODO: Unable to stop service
						#ifdef _DEBUG
						ILIBMESSAGE("Unable to stop service");
						#endif
					}
					else
					{
						Sleep(3000);
						r = 1;
					}
				}
			}
			CloseServiceHandle( service );
		}
		CloseServiceHandle( serviceControlManager );
	}
	return r;
}

int RunProcess(char* exe, int waitForExit)
{
	BOOL r = TRUE;
	int count = 50;
	DWORD exitcode;
	STARTUPINFOA info = {sizeof(info)};
	PROCESS_INFORMATION processInfo;
	if (CreateProcessA(NULL, exe, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &info, &processInfo) == 0) return 0;
	if (waitForExit != 0) {
		do
		{
			Sleep(100);
			r = GetExitCodeProcess(processInfo.hProcess, &exitcode);
			if (exitcode == STILL_ACTIVE) r = 0;
		} while (r == 0 && count-- > 0);
	}
	CloseHandle(processInfo.hProcess);
	CloseHandle(processInfo.hThread);
	return r;
}

/*
int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	return _tmain( 0, NULL );
}
*/

#ifndef _MINCORE
void fullinstall(int uninstallonly, char* proxy, int proxylen, char* tag, int taglen)
{
	int r = 0;
	int loops = 0;
	char targetexe2[_MAX_PATH + 40];
	char *targetexe = targetexe2 + 1;
	size_t targetexelen = 0;
	char selfexe[_MAX_PATH];
	size_t selfexelen = 0;
	char setup1[_MAX_PATH];
	char setup2[_MAX_PATH];
	int setup1len;
	int setup2len;

	if (IsAdmin() == FALSE) { printf("Requires administrator permissions.\r\n"); return; }
	if (uninstallonly != 0) { printf("Performing uninstall...\r\n"); } else { printf("Performing install...\r\n"); }

	HKEY hKey;
	DWORD len = 0;
	if (RegOpenKeyExA(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Services\\TRA Agent", 0, KEY_ALL_ACCESS, &hKey) == ERROR_SUCCESS)
	{
		if (RegQueryValueExA(hKey, "ImagePath", NULL, NULL, NULL, &len) == ERROR_SUCCESS && len > 0)
		{
			char *ipath = ILibMemory_Allocate(len, 0, NULL, NULL);
			RegQueryValueExA(hKey, "ImagePath", NULL, NULL, ipath, &len);
			printf("ImagePath: %s\n", ipath);

			STARTUPINFOA info = { sizeof(info) };
			PROCESS_INFORMATION processInfo;

			sprintf_s(ILibScratchPad, sizeof(ILibScratchPad), "%s -exec \"try { require('service-manager').manager.uninstallService('meshagentDiagnostic'); require('task-scheduler').delete('meshagentDiagnostic/periodicStart').then(function(){process.exit();}, function(){process.exit();}); } catch(e){process.exit();}\"", ipath);
			CreateProcessA(NULL, ILibScratchPad, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &info, &processInfo);
			CloseHandle(processInfo.hProcess);
			CloseHandle(processInfo.hThread);
			free(ipath);
		}
		RegCloseKey(hKey);
	}




	// Stop and remove the service
	StopService(serviceFile);

#ifdef TRA_DEBUG
	printf("StopService end.\r\n");
#endif

	// Wait for the service to stop
	int serviceStateLoopCount = 0;;
	int serviceState;
	do {
		serviceStateLoopCount++;
		Sleep(100);
		serviceState = GetServiceState(serviceFile);
	} while ((serviceState == 3) && (serviceStateLoopCount < 400));
	UninstallService(serviceFile);
	UninstallService(serviceFileOld);

	// Get our own executable
	selfexelen = GetModuleFileNameA(NULL, selfexe, _MAX_PATH);

#ifdef TRA_DEBUG
	printf("Get our own executable. [%s] \r\n", selfexe);
#endif

	// Get the target executable
	if (SHGetFolderPathA(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, targetexe) != S_FALSE)
	{
		targetexe2[0] = '\"';
		targetexelen = strnlen_s(targetexe, _MAX_PATH + 40);
		if (targetexelen <= MAX_PATH) memcpy_s(targetexe + targetexelen, _MAX_PATH + 40 - targetexelen, "\\TRA Agent\\TRAAgent.exe\" -uninstall", 38);
		//targetexelen += 25;
		// pjh 20190504
		targetexelen += 23;
	}

	// Check if we are uninstalling ourself
	if ((uninstallonly != 0) && (targetexelen == selfexelen) && (memcmp(selfexe, targetexe, targetexelen) == 0)) {
		// Copy ourself to a temp folder and run full uninstall.
		char tempPath[_MAX_PATH + 40];
		int tempPathLen = GetTempPathA(_MAX_PATH, tempPath);
		memcpy_s(tempPath + tempPathLen, _MAX_PATH + 40 - tempPathLen, "TRAAgent.exe\0", 15);
		remove(tempPath);
		util_CopyFile(selfexe, tempPath, FALSE);
		memcpy_s(tempPath + tempPathLen, _MAX_PATH + 40 - tempPathLen, "TRAAgent.exe -fulluninstall\0", 30);
		RunProcess(tempPath, 0); // Don't wait for the process to terminate since we want to self-delete.
		return;
	}

#ifdef TRA_DEBUG
	printf("Call uninstall, this will remove the firewall rules. [%s] \r\n", targetexe2);
#endif

	// Call uninstall, this will remove the firewall rules.
	RunProcess(targetexe2, 1);

#ifdef _MINCORE
	// Remove the MeshAgent registry keys
	RegDeleteKeyEx(HKEY_LOCAL_MACHINE, "Software\\Thingscare\\TRAAgent", KEY_WOW64_32KEY, 0);
	RegDeleteKeyEx(HKEY_CURRENT_USER, "Software\\Thingscare\\TRAAgent", KEY_WOW64_32KEY, 0);
#else
	// Remove the MeshAgent registry keys
	RegDeleteKey(HKEY_LOCAL_MACHINE, "Software\\Thingscare\\TRAAgent");
	RegDeleteKey(HKEY_CURRENT_USER, "Software\\Thingscare\\TRAAgent");
#endif

#ifdef TRA_DEBUG
	printf("Remove the uninstall icon from the control panel if present \r\n");
#endif

	// Remove the uninstall icon from the control panel if present
	RemoveUninstallIcon();

#ifdef TRA_DEBUG
	printf("Remove the uninstall icon from the control panel if present end.\r\n");
#endif

	// Check if selfexe is already located at the target, if so, skip to copy steps.
	if ((uninstallonly != 0) || (targetexelen != selfexelen) || (memcmp(selfexe, targetexe, targetexelen) != 0))
	{
		// Remove the target executable, wait if needed
		int selfExeDelLoopCount = 0;;
		int selfExeDel;
		targetexe[targetexelen] = 0;
		do {
			Sleep(100);
			selfExeDelLoopCount++;
			selfExeDel = remove(targetexe);
		} while ((selfExeDel != 0) && (selfExeDel != -1) && (selfExeDelLoopCount < 400));

#ifdef TRA_DEBUG
		printf("Remove the target executable, wait if needed [%s] \r\n", targetexe);
#endif

		// Remove "[Executable].msh" file
		if ((setup2len = (int)strnlen_s(targetexe, _MAX_PATH + 40)) < 4 || setup2len > 259) return;
		memcpy_s(setup2, sizeof(setup2), targetexe, setup2len);
		memcpy_s(setup2 + (setup2len - 3), sizeof(setup2) - setup2len - 3, "msh", 4);
		setup2[setup2len] = 0;
		remove(setup2);

		// Remove "[Executable].mshx" file
		if ((setup2len = (int)strnlen_s(targetexe, _MAX_PATH + 40)) < 4 || setup2len > 259) return;
		memcpy_s(setup2, sizeof(setup2), targetexe, setup2len);
		memcpy_s(setup2 + (setup2len - 3), sizeof(setup2) - setup2len - 3, "mshx", 5);
		setup2[setup2len + 1] = 0;
		remove(setup2);


		// Remove "[Executable].proxy" file
		if ((setup2len = (int)strnlen_s(targetexe, _MAX_PATH + 40)) < 4 || setup2len > 257) return;
		memcpy_s(setup2, sizeof(setup2), targetexe, setup2len);
		memcpy_s(setup2 + (setup2len - 3), sizeof(setup2) - setup2len - 3, "proxy", 6);
		setup2[setup2len + 2] = 0;
		remove(setup2);

		// Remove "[Executable].tag" file
		if ((setup2len = (int)strnlen_s(targetexe, _MAX_PATH + 40)) < 4 || setup2len > 259) return;
		memcpy_s(setup2, sizeof(setup2), targetexe, setup2len);
		memcpy_s(setup2 + (setup2len - 3), sizeof(setup2) - setup2len - 3, "tag", 4);
		setup2[setup2len] = 0;
		remove(setup2);

		// Remove "[Executable].log" file
		if ((setup2len = (int)strnlen_s(targetexe, _MAX_PATH + 40)) < 4 || setup2len > 259) return;
		memcpy_s(setup2, sizeof(setup2), targetexe, setup2len);
		memcpy_s(setup2 + (setup2len - 3), sizeof(setup2) - setup2len - 3, "log", 4);
		setup2[setup2len] = 0;
		remove(setup2);

		// Remove "[Executable].db" file
		if ((setup2len = (int)strnlen_s(targetexe, _MAX_PATH + 40)) < 4 || setup2len > 256) return;
		memcpy_s(setup2, sizeof(setup2), targetexe, setup2len);
		memcpy_s(setup2 + (setup2len - 3), sizeof(setup2) - setup2len - 3, "db", 3);
		setup2[setup2len] = 0;
		remove(setup2);

		// Remove the folder.
		//targetexe[targetexelen - 14] = 0;
		// pjh 20190504
		targetexe[targetexelen - 13] = 0;
		RemoveDirectoryA(targetexe);

		if (uninstallonly != 0) return;

		// Get the target executable, create folders if needed
		if (!CreateDirectoryA(targetexe, NULL) && GetLastError() == ERROR_ACCESS_DENIED) { ILIBMESSAGE("Access denied (1)"); return; }
		//targetexe[targetexelen - 14] = '\\';
		// pjh 20190504
		targetexe[targetexelen - 13] = '\\';


		// Attempt to copy our own exe over the original exe
		loops = 0;
		while (!util_CopyFile(selfexe, targetexe, TRUE))
		{
#ifdef TRA_DEBUG
			printf("Failed util_CopyFile [%s][%s]\r\n", selfexe, targetexe);
#endif

			if (GetLastError() == ERROR_ACCESS_DENIED) { ILIBMESSAGE("Access denied (2)"); return; }
			if (loops++ > 5) { ILIBMESSAGE("Error copying executable file"); return; }
			Sleep(5000);
		}

		// Try to copy "[Executable].msh" file to target directory
		if ((setup1len = (int)strnlen_s(selfexe, sizeof(selfexe))) < 4) return;
		memcpy_s(setup1, sizeof(setup1), selfexe, setup1len);
		memcpy_s(setup1 + (setup1len - 3), sizeof(setup1) - setup1len - 3, "msh", 4);
		if ((setup2len = (int)strnlen_s(targetexe, _MAX_PATH + 40)) < 4 || setup2len > 259) return;
		memcpy_s(setup2, sizeof(setup2), targetexe, setup2len);
		memcpy_s(setup2 + (setup2len - 3), sizeof(setup2) - setup2len - 3, "msh", 4);
		util_CopyFile(setup1, setup2, TRUE);

		// Write the tag if one was passed
		if (tag != NULL)
		{
			FILE *SourceFile = NULL;
			if ((setup2len = (int)strnlen_s(targetexe, _MAX_PATH + 40)) < 4 || setup2len > 259) return;
			memcpy_s(setup2, sizeof(setup2), targetexe, setup2len);
			memcpy_s(setup2 + (setup2len - 3), sizeof(setup2) - setup2len - 3, "tag", 4);
			if (taglen > 0) {
				fopen_s(&SourceFile, setup2, "wb");
				if (SourceFile != NULL)
				{
					if (fwrite(tag, sizeof(char), taglen, SourceFile)) {}
					fclose(SourceFile);
				}
			}
			else
			{
				remove(setup2);
			}
		}

		// Setup proxy filenames
		if ((setup1len = (int)strnlen_s(selfexe, sizeof(selfexe))) < 4) return;
		memcpy_s(setup1, sizeof(setup1), selfexe, setup1len);
		memcpy_s(setup1 + (setup1len - 3), sizeof(setup1) - setup1len - 3, "proxy", 6);
		if ((setup2len = (int)strnlen_s(targetexe, _MAX_PATH + 40)) < 4 || setup2len > 259) return;
		memcpy_s(setup2, sizeof(setup2), targetexe, setup2len);
		memcpy_s(setup2 + (setup2len - 3), sizeof(setup2) - setup2len - 3, "proxy", 6);

		if (proxy != NULL && proxylen > 0)
		{
			// Use the specified proxy in the command line switch
			FILE *SourceFile = NULL;
			fopen_s(&SourceFile, setup2, "wb");
			if (SourceFile != NULL)
			{
				if (fwrite(proxy, sizeof(char), proxylen, SourceFile)) {}
				fclose(SourceFile);
			}
		}
		else
		{
			// Try to copy "[Executable].proxy" file to target directory
			if (util_CopyFile(setup1, setup2, TRUE) == FALSE)
			{
				// Failed to copy proxy file, lets try to create one.
				WINHTTP_CURRENT_USER_IE_PROXY_CONFIG proxyEx;
				if (WinHttpGetIEProxyConfigForCurrentUser(&proxyEx))
				{
					if (proxyEx.lpszProxy != NULL)
					{
						FILE *SourceFile = NULL;
						size_t len;
						if (wcstombs_s(&len, ILibScratchPad, 4095, proxyEx.lpszProxy, 2000) == 0)
						{
							char* ptr = strstr(ILibScratchPad, "https=");
							if (ptr != NULL) {
								char* ptr2 = strstr(ptr, ";");
								ptr += 6;
								if (ptr2 != NULL) ptr2[0] = 0;
							} else {
								ptr = ILibScratchPad;
							}
							fopen_s(&SourceFile, setup2, "wb");
							if (SourceFile != NULL)
							{
								if (fwrite(ptr, sizeof(char), strnlen_s(ptr, sizeof(ILibScratchPad)), SourceFile)) {}
								fclose(SourceFile);
							}
						}
						GlobalFree(proxyEx.lpszProxy);
					}

					// Release the rest of the proxy settings
					if (proxyEx.lpszAutoConfigUrl != NULL) GlobalFree(proxyEx.lpszAutoConfigUrl);
					if (proxyEx.lpszProxyBypass != NULL) GlobalFree(proxyEx.lpszProxyBypass);
				}
			}
		}
	}

#ifdef TRA_DEBUG
	printf("Add the uninstall icon in the control panel \r\n");
#endif

	// Add the uninstall icon in the control panel
	AddUninstallIcon();

	/*
#if defined(_LINKVM)
	// Setup the SendSAS permission
	kvm_setupSasPermissions();
#endif
	*/

	// Attempt to start the updated service up again
	memcpy(targetexe + targetexelen, "\" -install", 11);
	r = RunProcess(targetexe2, 1);
	memcpy(targetexe + targetexelen, "\" -start", 9);
	r = RunProcess(targetexe2, 1);
}
#endif


ILibTransport_DoneState kvm_serviceWriteSink(char *buffer, int bufferLen, void *reserved)
{
	DWORD len;
	WriteFile(GetStdHandle(STD_OUTPUT_HANDLE), buffer, bufferLen, &len, NULL);
	return ILibTransport_DoneState_COMPLETE;
}
BOOL CtrlHandler(DWORD fdwCtrlType)
{
	switch (fdwCtrlType)
	{
		// Handle the CTRL-C signal. 
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	{
		if (agent != NULL) { MeshAgent_Stop(agent); }
		return TRUE;
	}
	default:
		return FALSE;
	}
}

#define wmain_free(argv) for(argvi=0;argvi<(int)(ILibMemory_Size(argv)/sizeof(void*));++argvi){ILibMemory_Free(argv[argvi]);}ILibMemory_Free(argv);
int wmain(int argc, char* wargv[])
{
	int i;
	size_t str2len = 0;// , proxylen = 0, taglen = 0;
	wchar_t str[_MAX_PATH];
	char str2[_MAX_PATH];
	char* proxyarg = NULL;
	char* tagarg = NULL;
	ILib_DumpEnabledContext winException;
	int retCode = 0;

	int argvi, argvsz;
	char **argv = (char**)ILibMemory_SmartAllocate(argc * sizeof(void*));
	for (argvi = 0; argvi < argc; ++argvi)
	{
		argvsz = WideCharToMultiByte(CP_UTF8, 0, (LPCWCH)wargv[argvi], -1, NULL, 0, NULL, NULL);
		argv[argvi] = (char*)ILibMemory_SmartAllocate(argvsz);
		WideCharToMultiByte(CP_UTF8, 0, (LPCWCH)wargv[argvi], -1, argv[argvi], argvsz, NULL, NULL);
	}

	/*
#ifndef NOMESHCMD
	// Check if this is a Mesh command operation
	if (argc >= 1 && strlen(argv[0]) >= 7 && strcasecmp(argv[0] + strlen(argv[0]) - 7, "meshcmd") == 0) return MeshCmd_ProcessCommand(argc, argv, 1);
	if (argc >= 2 && strcasecmp(argv[1], "meshcmd") == 0) return MeshCmd_ProcessCommand(argc, argv, 2);
#endif
	*/

	//CoInitializeEx(NULL, COINIT_MULTITHREADED);

	if (argc > 1 && (strcasecmp(argv[1], "-test") == 0))
	{
		/*
		char targettext[_MAX_PATH + 40];
		char resulttext[_MAX_PATH + 40];

		strcpy(targettext, "sender.exe.update");
		setFullFilePath(targettext);

		strcpy(resulttext, "sender.exe");
		setFullFilePath(resulttext);

		renameFile(targettext, resulttext, FALSE);
		*/
		wmain_free(argv);
		return(0);
	}


	// pjh 20190330
	if (argc > 1 && ((strcasecmp(argv[1], "-gui") == 0) || (strcasecmp(argv[1], "-allowremote") == 0)))
	{
		HWND hWndConsole = GetConsoleWindow();
		ShowWindow(hWndConsole, SW_HIDE);

		g_serviceMode = FALSE;
	}
	else
	{
		// pjh 20190328 Red Dot (Run mode)
		g_hres = GetModuleHandle(NULL);
		create_rc_wnds();
	}

	if (argc > 1 && strcasecmp(argv[1], "-info") == 0)
	{
		printf("Compiled on: %s, %s\n", __TIME__, __DATE__);
		if (SOURCE_COMMIT_HASH != NULL && SOURCE_COMMIT_DATE != NULL)
		{
			printf("   Commit Hash: %s\n", SOURCE_COMMIT_HASH);
			printf("   Commit Date: %s\n", SOURCE_COMMIT_DATE);
		}
#ifndef MICROSTACK_NOTLS
		printf("Using %s\n", SSLeay_version(SSLEAY_VERSION));
#endif
		wmain_free(argv);
		return(0);
	}

	if (argc > 2 && strcasecmp(argv[1], "-faddr") == 0)
	{
#ifdef WIN64
		uint64_t addrOffset = 0;
		sscanf_s(argv[2] + 2, "%016llx", &addrOffset);
#else
		uint32_t addrOffset = 0;
		sscanf_s(argv[2] + 2, "%x", &addrOffset);
#endif
		ILibChain_DebugOffset(ILibScratchPad, sizeof(ILibScratchPad), (uint64_t)addrOffset);
		printf("%s", ILibScratchPad);
		wmain_free(argv);
		return(0);
	}

	if (argc > 2 && strcasecmp(argv[1], "-fdelta") == 0)
	{
		uint64_t delta = 0;
		sscanf_s(argv[2], "%lld", &delta);
		ILibChain_DebugDelta(ILibScratchPad, sizeof(ILibScratchPad), delta);
		printf("%s", ILibScratchPad);
		wmain_free(argv);
		return(0);
	}

	char *integratedJavaScript;
	int integragedJavaScriptLen;
	ILibDuktape_ScriptContainer_CheckEmbedded(&integratedJavaScript, &integragedJavaScriptLen);
	if (argc > 2 && strcmp(argv[1], "-exec") == 0 && integragedJavaScriptLen == 0)
	{
		integratedJavaScript = ILibString_Copy(argv[2], -1);
		integragedJavaScriptLen = (int)strnlen_s(integratedJavaScript, sizeof(ILibScratchPad));
	}
	if (argc > 2 && strcmp(argv[1], "-b64exec") == 0 && integragedJavaScriptLen == 0)
	{
		integragedJavaScriptLen = ILibBase64Decode((unsigned char *)argv[2], (const int)strnlen_s(argv[2], sizeof(ILibScratchPad2)), (unsigned char**)&integratedJavaScript);
	}
	if (argc > 1 && strcasecmp(argv[1], "-nodeid") == 0)
	{
		char script[] = "console.log(require('_agentNodeId')());process.exit();";
		integratedJavaScript = ILibString_Copy(script, (int)sizeof(script) - 1);
		integragedJavaScriptLen = (int)sizeof(script) - 1;
	}

	CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

	// Process extra switches
	for (i = 0; i < argc; i++)
	{
		str2len = strnlen_s(argv[i], _MAX_PATH - 1);
		if (str2len > 7 && memcmp(argv[i], "-proxy:", 7) == 0) { proxyarg = argv[i] + 7; } // Set the HTTPS proxy
		else if (str2len >= 5 && memcmp(argv[i], "-tag:", 5) == 0) { tagarg = argv[i] + 5; } // Set the TAG
	}

	/*
	#ifdef _MINCORE
	if (argc == 2 && ((strcasecmp(argv[1], "-?") == 0) || (strcasecmp(argv[1], "/?") == 0)))
	{
		printf("Mesh Agent v%d available switches:\r\n  start             Start the service.\r\n  restart           Restart the service.\r\n  stop              Stop the service.\r\n  -signcheck        Perform self-check.\r\n  -install          Install the service from this location.\r\n  -uninstall        Remove the service from this location.\r\n  -nodeidhex        Return the current agent identifier.\r\n  -proxy:host:port  Specifiy an HTTPS proxy (after -fullinstall only).\r\n  -tag:xxx          Specifiy a agent tag  (after -fullinstall only).\r\n\r\n  -resetnodeid      Reset the NodeID next time the service is started.", MESH_AGENT_VERSION);
		return 0;
	}
	#else
	if (argc == 2 && ((strcasecmp(argv[1], "-?") == 0) || (strcasecmp(argv[1], "/?") == 0)))
	{
		//printf("Mesh Agent v%d available switches:\r\n  start             Start the service.\r\n  restart           Restart the service.\r\n  stop              Stop the service.\r\n  -proxy:host:port  Specifiy an HTTPS proxy.\r\n  -leader           Force the agent to always be a leader.\r\n  -signcheck        Perform self-check.\r\n  -install          Install the service from this location.\r\n  -uninstall        Remove the service from this location.\r\n  -nodeidhex        Return the current agent identifier.\r\n  -fullinstall      Copy agent into program files, install and launch.\r\n  -fulluninstall    Stop agent and clean up the program files location.\r\n  -loadcert:c.pem   Load a pem cert as node certificate.\r\n", MESH_AGENT_VERSION);
		printf("Mesh Agent v%d available switches:\r\n  start             Start the service.\r\n  restart           Restart the service.\r\n  stop              Stop the service.\r\n  -signcheck        Perform self-check.\r\n  -install          Install the service from this location.\r\n  -uninstall        Remove the service from this location.\r\n  -nodeidhex        Return the current agent identifier.\r\n  -fullinstall      Copy agent into program files, install and launch.\r\n  -fulluninstall    Stop agent and clean up the program files location.\r\n  -proxy:host:port  Specifiy an HTTPS proxy (after -fullinstall only).\r\n  -tag:xxx          Specifiy a agent tag  (after -fullinstall only).\r\n  -resetnodeid      Reset the NodeID next time the service is started.", MESH_AGENT_VERSION);
		return 0;
	}
	#endif
	*/

	#if defined(_LINKVM)
	if (argc > 1 && strcasecmp(argv[1], "-kvm0") == 0)
	{		
		void **parm = (void**)ILibMemory_Allocate(3 * sizeof(void*), 0, 0, NULL);
		parm[0] = kvm_serviceWriteSink;
		((int*)&(parm[2]))[0] = 0;

		HMODULE shCORE = LoadLibraryExA((LPCSTR)"Shcore.dll", NULL, LOAD_LIBRARY_SEARCH_SYSTEM32);
		DpiAwarenessFunc dpiAwareness = NULL;
		if (shCORE != NULL)
		{
			if ((dpiAwareness = (DpiAwarenessFunc)GetProcAddress(shCORE, (LPCSTR)"SetProcessDpiAwareness")) == NULL)
			{
				FreeLibrary(shCORE);
				shCORE = NULL;
			}
		}
		if (dpiAwareness != NULL)
		{
			dpiAwareness(PROCESS_PER_MONITOR_DPI_AWARE);
			FreeLibrary(shCORE);
			shCORE = NULL;
		}
		else
		{
			SetProcessDPIAware();
		}

		// 20190729
		WORD wVersionRequested = MAKEWORD(2, 0);  // WinSock 2.0 夸没
		WSADATA wsaData;
		int nErrorStatus;

		nErrorStatus = WSAStartup(wVersionRequested, &wsaData);

		g_kvm_type = 0;
		m_ThreadKVM = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(DoKVMRecv), (LPVOID)NULL, 0, &threadKVMID);
		ResumeThread(m_ThreadKVM);

		kvm_server_mainloop((void*)parm);
		wmain_free(argv);
		return 0;
	}
	else if (argc > 1 && strcasecmp(argv[1], "-kvm1") == 0)
	{
		void **parm = (void**)ILibMemory_Allocate(3 * sizeof(void*), 0, 0, NULL);
		parm[0] = kvm_serviceWriteSink;
		((int*)&(parm[2]))[0] = 1;

		HMODULE shCORE = LoadLibraryExA((LPCSTR)"Shcore.dll", NULL, LOAD_LIBRARY_SEARCH_SYSTEM32);
		DpiAwarenessFunc dpiAwareness = NULL;
		if (shCORE != NULL)
		{
			if ((dpiAwareness = (DpiAwarenessFunc)GetProcAddress(shCORE, (LPCSTR)"SetProcessDpiAwareness")) == NULL)
			{
				FreeLibrary(shCORE);
				shCORE = NULL;
			}
		}
		if (dpiAwareness != NULL)
		{
			dpiAwareness(PROCESS_PER_MONITOR_DPI_AWARE);
			FreeLibrary(shCORE);
			shCORE = NULL;
		}
		else
		{
			SetProcessDPIAware();
		}

		// 20190729
		WORD wVersionRequested = MAKEWORD(2, 0);  // WinSock 2.0 夸没
		WSADATA wsaData;
		int nErrorStatus;

		nErrorStatus = WSAStartup(wVersionRequested, &wsaData);

		g_kvm_type = 1;
		m_ThreadKVM = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(DoKVMRecv), (LPVOID)NULL, 0, &threadKVMID);
		ResumeThread(m_ThreadKVM);

		kvm_server_mainloop((void*)parm);
		wmain_free(argv);
		return 0;
	}
	#endif

#ifdef _MINCORE
	if (argc > 1 && strcasecmp(argv[1], "-signcheck") == 0)
	{
		// Check the signature of out own executable
		util_openssl_init();
		printf("%d", signcheck_verifysign(argv[0], 0));
		util_openssl_uninit();
		return 0;
	}
#else
	
	if (integratedJavaScript != NULL || (argc > 0 && strcasecmp(argv[0], "--slave") == 0) || (argc > 1 && ((strcasecmp(argv[1], "run") == 0) || (strcasecmp(argv[1], "connect") == 0) || (strcasecmp(argv[1], "--slave") == 0))))
	{
		g_serviceMode = FALSE;

		// Run the mesh agent in console mode, since the agent is compiled for windows service, the KVM will not work right. This is only good for testing.
		SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE); // Set SIGNAL on windows to listen for Ctrl-C

		__try
		{
			int capabilities = 0;
			if (argc > 1 && ((strcasecmp(argv[1], "connect") == 0))) { capabilities = MeshCommand_AuthInfo_CapabilitiesMask_TEMPORARY; }
			agent = MeshAgent_Create(capabilities);
			agent->meshCoreCtx_embeddedScript = integratedJavaScript;
			agent->meshCoreCtx_embeddedScriptLen = integragedJavaScriptLen;
			if (integratedJavaScript != NULL || (argc > 1 && (strcasecmp(argv[1], "run") == 0 || strcasecmp(argv[1], "connect") == 0))) { agent->runningAsConsole = 1; }
			MeshAgent_Start(agent, argc, argv);
			retCode = agent->exitCode;
			MeshAgent_Destroy(agent);
			agent = NULL;
		}
		__except (ILib_WindowsExceptionFilterEx(GetExceptionCode(), GetExceptionInformation(), &winException))
		{
			ILib_WindowsExceptionDebugEx(&winException);
		}
		wmain_free(argv);
		return(retCode);
	}
	else if (argc > 1 && (strcasecmp(argv[1], "state") == 0))
	{
		// SERVICE_STOPPED				  1    The service is not running.
		// SERVICE_START_PENDING		  2    The service is starting.
		// SERVICE_STOP_PENDING			  3    The service is stopping.
		// SERVICE_RUNNING				  4    The service is running.
		// SERVICE_CONTINUE_PENDING		  5    The service continue is pending.
		// SERVICE_PAUSE_PENDING		  6    The service pause is pending.
		// SERVICE_PAUSED				  7    The service is paused.
		// SERVICE_NOT_INSTALLED		100    The service is not installed.
		int serviceState = GetServiceState(serviceFile);
		if (serviceState == 1) { printf("Stopped"); }
		else if (serviceState == 2) { printf("Start Pending"); }
		else if (serviceState == 3) { printf("Stop Pending"); }
		else if (serviceState == 4) { printf("Running"); }
		else if (serviceState == 5) { printf("Continue Pending"); }
		else if (serviceState == 6) { printf("Pause Pending"); }
		else if (serviceState == 7) { printf("Paused"); }
		else if (serviceState == 100) { printf("Not installed"); }
		wmain_free(argv);
		return serviceState;
	}
	else if (argc > 1 && strcasecmp(argv[1], "-signcheck") == 0 && GetModuleFileNameA(NULL, str2, _MAX_PATH) > 5)
	{
		// Check the signature of out own executable
#ifndef MICROSTACK_NOTLS
		util_openssl_init();
		printf("%d", signcheck_verifysign(str2, 0));
		util_openssl_uninit();
#else
		printf("Cannot verify without OpenSSL support");
#endif
		wmain_free(argv);
		return 0;
	}
#endif
	else if (argc > 1 && (strcasecmp(argv[1], "start") == 0 || strcasecmp(argv[1], "-start") == 0))
	{
		// Ask the service manager to launch the service
		int r = LaunchService(serviceFile);
		if (r == 0) { printf("Failed to start mesh agent"); }
		else if (r == 1) { printf("Started the mesh agent"); }
		else if (r == 2) { printf("Mesh agent already running"); }
	}
	else if (argc > 1 && (strcasecmp(argv[1], "stop") == 0 || strcasecmp(argv[1], "-stop") == 0))
	{
		// Ask the service manager to stop the service
		if (StopService(serviceFile) == 1) { printf("Stopped mesh agent"); } else { printf("Failed to stop mesh agent"); }
	}
	else if (argc > 1 && (strcasecmp(argv[1], "restart") == 0 || strcasecmp(argv[1], "-restart") == 0))
	{
		// Ask the service manager to stop and start the service
		StopService(serviceFile);
		{
			int r = LaunchService(serviceFile);
			if (r == 0) { printf("Failed to restart mesh agent"); }
			else if (r == 1) { printf("Restarted the mesh agent"); }
			else if (r == 2) { printf("Mesh agent failed to stop"); }
		}
	}
	else if (argc > 1 && strcasecmp(argv[1], "-install") == 0)
	{
		// Setup the service
		StopService(serviceFile);
		UninstallService(serviceFile);
		UninstallService(serviceFileOld);
		if (InstallService() == TRUE) { printf("Mesh agent installed"); } else { printf("Failed to install mesh agent"); }

#ifndef _MINCORE
		// Setup the Windows firewall
		if (GetModuleFileNameW(NULL, str, _MAX_PATH) > 5)
		{
			ClearWindowsFirewall(str);
			if (SetupWindowsFirewall(str) != 0)
			{
				#ifdef _DEBUG
				ILIBMESSAGE("Firewall rules added successfully");
				#endif
			}
			else
			{
				#ifdef _DEBUG
				ILIBMESSAGE("Unable to add firewall rules");
				#endif
			}
		}
#endif
	}
	else if (argc > 1 && ((strcasecmp(argv[1], "-remove") == 0) || (strcasecmp(argv[1], "-uninstall") == 0)))
	{
		// Ask the service manager to stop the service
		StopService(serviceFile);

		// Remove the service
		UninstallService(serviceFileOld);
		i = UninstallService(serviceFile);
		if (i == 0) { printf("Failed to uninstall mesh agent"); }
		else if (i == 1) { printf("Mesh agent uninstalled"); }
		else if (i == 2) { printf("Mesh agent still running"); }

		// 20190424

#ifndef _MINCORE
		// Remove the MeshAgent registry keys
		RegDeleteKey(HKEY_LOCAL_MACHINE, "Software\\Thingscare\\TRAAgent");
		RegDeleteKey(HKEY_CURRENT_USER, "Software\\Thingscare\\TRAAgent");

		// Cleanup the firewall rules
		if (GetModuleFileNameW( NULL, str, _MAX_PATH ) > 5)
		{
			if (ClearWindowsFirewall(str) != 0)
			{
				#ifdef _DEBUG
				ILIBMESSAGE("Firewall rules removed successfully");
				#endif
			}
			else
			{
				#ifdef _DEBUG
				ILIBMESSAGE("Unable to remove firewall rules");
				#endif
			}
		}
#endif
	}
#ifdef _MINCORE
	else if (argc > 1 && memcmp(argv[1], "-update:", 8) == 0)
	{
		// Attempt to copy our own exe over the original exe
		while (util_CopyFile(argv[0], argv[1] + 8, FALSE) == FALSE) { Sleep(5000); }

		// Attempt to start the updated service up again
		LaunchService();
	}
#endif
#ifndef _MINCORE
	else if (argc > 1 && memcmp(argv[1], "-update:", 8) == 0)
	{
		// Attempt to copy our own exe over the original exe
		while (util_CopyFile(argv[0], argv[1] + 8, FALSE) == FALSE) Sleep(5000);

		// Attempt to start the updated service up again
		LaunchService(serviceFile);
	}
	else if (argc > 1 && (strcasecmp(argv[1], "-netinfo") == 0))
	{
		char* data;
		int len = MeshInfo_GetSystemInformation(&data);
		if (len > 0) { printf(data); }
	}
	else if (argc > 1 && (strcasecmp(argv[1], "-fullinstall") == 0))
	{
		// 20191023 pjh
		if (g_me_version)
		{
			g_timer_shutdown = TRUE;
			g_timer_use = FALSE;
		}

		fullinstall( 0, proxyarg, (int)strnlen_s(proxyarg, _MAX_PATH), tagarg, (int)strnlen_s(tagarg, _MAX_PATH));
	}
	else if (argc > 1 && (strcasecmp(argv[1], "-fulluninstall") == 0))
	{
		// 20191023 pjh
		if (g_me_version)
		{
			g_timer_shutdown = TRUE;
			g_timer_use = FALSE;
		}

		// Remote Tray Process 20190803 - pjh
		WORD wVersionRequested = MAKEWORD(2, 0);  // WinSock 2.0 夸没
		WSADATA wsaData;
		int nErrorStatus;

		nErrorStatus = WSAStartup(wVersionRequested, &wsaData);

		UDPMSG udpmsg;
		UdpInitGui();
		UdpConnectGui(UDPPROC_GUI_PORT);
		memset((void*)&udpmsg, 0x00, sizeof(udpmsg));
		udpmsg.command = UDP_GUI_TERMINATE;
		UdpSendGui(udpmsg);

		Sleep(300);

		fullinstall(1, NULL, 0, NULL, 0);
	}
	else if (argc > 1 && (strcasecmp(argv[1], "-setfirewall") == 0))
	{
		// Reset the firewall rules
		GetModuleFileNameW(NULL, str, _MAX_PATH);
		if (IsAdmin() == FALSE) { printf("Must run as administrator"); } else { ClearWindowsFirewall(str); SetupWindowsFirewall(str); printf("Done"); }
	}
	else if (argc > 1 && (strcasecmp(argv[1], "-clearfirewall") == 0))
	{
		// Clear the firewall rules
		GetModuleFileNameW(NULL, str, _MAX_PATH);
		if (IsAdmin() == FALSE) { printf("Must run as administrator"); } else { ClearWindowsFirewall(str); printf("Done"); }
	}
#endif
	else if (argc == 2 && (strcasecmp(argv[1], "-nodeidhex") == 0))
	{
		// Get the NodeID from the registry
		HKEY hKey;
		DWORD len = 0;
		char* strEx = NULL;
#ifndef _WIN64
		if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\Thingscare\\TRAAgent", 0, KEY_QUERY_VALUE | KEY_WOW64_32KEY, &hKey) == ERROR_SUCCESS )
#else
		if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\Thingscare\\TRAAgent", 0, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS )
#endif
		{
			if (RegQueryValueExA(hKey, "NodeId", NULL, NULL, NULL, &len ) == ERROR_SUCCESS && len > 0)
			{
				if ((strEx = (char*)malloc(len)) == NULL) ILIBCRITICALEXIT(254);
				if (RegQueryValueExA(hKey, "NodeId", NULL, NULL, (LPBYTE)strEx, &len ) != ERROR_SUCCESS || len == 0) { free(strEx); strEx = NULL; len = 0;}
			}
			RegCloseKey(hKey);
		}
		if (strEx != NULL) printf(strEx); else printf("Not defined, start the mesh service to create a nodeid.");
		wmain_free(argv);
		return 0;
	}
	else if (argc == 2 && (strcasecmp(argv[1], "-info") == 0))
	{
		// Display agent information from the registry
		HKEY hKey;
		DWORD len = 0;
		char* strEx = NULL;
#ifndef _WIN64
		if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\Thingscare\\TRAAgent", 0, KEY_QUERY_VALUE | KEY_WOW64_32KEY, &hKey) == ERROR_SUCCESS)
#else
		if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\Thingscare\\TRAAgent", 0, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS)
#endif
		{
			// Display NodeId
			len = sizeof(ILibScratchPad2);
			if (RegQueryValueExA(hKey, "NodeId", NULL, NULL, (LPBYTE)ILibScratchPad2, &len) != ERROR_SUCCESS) { len = 0; }
			if (len == 0) printf("NodeId:    (none)"); else printf("NodeId:    %s", ILibScratchPad2);

			// Display MeshId
			len = sizeof(ILibScratchPad2);
			if (RegQueryValueExA(hKey, "MeshId", NULL, NULL, (LPBYTE)ILibScratchPad2, &len) != ERROR_SUCCESS) { len = 0; }
			if (len > 0) printf("\r\nMeshId:    %s", ILibScratchPad2);

			// Display AgentHash
			len = sizeof(ILibScratchPad2);
			if (RegQueryValueExA(hKey, "AgentHash", NULL, NULL, (LPBYTE)ILibScratchPad2, &len) != ERROR_SUCCESS) { len = 0; }
			if (len > 0) printf("\r\nAgentHash: %s", ILibScratchPad2);

			// Display MeshServerId
			len = sizeof(ILibScratchPad2);
			if (RegQueryValueExA(hKey, "MeshServerId", NULL, NULL, (LPBYTE)ILibScratchPad2, &len) != ERROR_SUCCESS) { len = 0; }
			if (len > 0) printf("\r\nServerId:  %s", ILibScratchPad2);

			// Display MeshServerUrl
			len = sizeof(ILibScratchPad2);
			if (RegQueryValueExA(hKey, "MeshServerUrl", NULL, NULL, (LPBYTE)ILibScratchPad2, &len) != ERROR_SUCCESS) { len = 0; }
			if (len > 0) printf("\r\nServerUrl: %s", ILibScratchPad2);

			// Display Proxy
			len = sizeof(ILibScratchPad2);
			if (RegQueryValueExA(hKey, "Proxy", NULL, NULL, (LPBYTE)ILibScratchPad2, &len) != ERROR_SUCCESS) { len = 0; }
			if (len > 0) printf("\r\nProxy:     %s", ILibScratchPad2);

			// Display Tag
			len = sizeof(ILibScratchPad2);
			if (RegQueryValueExA(hKey, "Tag", NULL, NULL, (LPBYTE)ILibScratchPad2, &len) != ERROR_SUCCESS) { len = 0; }
			if (len > 0) printf("\r\nTag:       %s", ILibScratchPad2);

			RegCloseKey(hKey);
		}
		wmain_free(argv);
		return 0;
	}
	else if (argc == 2 && (strcasecmp(argv[1], "-resetnodeid") == 0))
	{
		// Set "resetnodeid" in registry
		HKEY hKey;
#ifndef _WIN64
		if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\Thingscare\\TRAAgent", 0, KEY_WRITE | KEY_WOW64_32KEY, &hKey) == ERROR_SUCCESS )
#else
		if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\Thingscare\\TRAAgent", 0, KEY_WRITE, &hKey) == ERROR_SUCCESS )
#endif
		{
			i = 1;
			DWORD err = RegSetValueEx(hKey, "ResetNodeId", 0, REG_DWORD, (BYTE*)&i, (DWORD)4);
			if (err == ERROR_SUCCESS) { printf("NodeID will be reset next time the Mesh Agent service is started."); }
			RegCloseKey(hKey);
		}
		else
		{
			printf("Error writing to registry, try running as administrator.");
		}
		wmain_free(argv);
		return 0;
	}
	else if (argc > 1 && (strcasecmp(argv[1], "-gui") == 0))
	{
		g_hres = GetModuleHandle(NULL);

		create_rc_wnds();

		FreeConsole();

		g_serviceMode = FALSE;

		WORD wVersionRequested = MAKEWORD(2, 0);  // WinSock 2.0 夸没
		WSADATA wsaData;
		int nErrorStatus;

		nErrorStatus = WSAStartup(wVersionRequested, &wsaData);

		// 201907 Tray
		MyRegisterClass(g_hres);
		InitInstance(g_hres, 1);

		// 20190424 pjh
		m_ThreadGui = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(DoUdpGuiRecv), (LPVOID)NULL, 0, &threadGuiID);
		ResumeThread(m_ThreadGui);

		//InitCursorShapeDetector();

		DialogBox(NULL, MAKEINTRESOURCE(IDD_GUI_DEFAULT), NULL, GuiDialogHandler);

	}
	else if (argc > 1 && (strcasecmp(argv[1], "-allowremote") == 0))
	{
		g_hres = GetModuleHandle(NULL);
		FreeConsole();

		g_serviceMode = FALSE;

		g_socketNumber = atoi(argv[2]);

		WORD wVersionRequested = MAKEWORD(2, 0);  // WinSock 2.0 夸没
		WSADATA wsaData;
		int nErrorStatus;

		nErrorStatus = WSAStartup(wVersionRequested, &wsaData);

		if (nErrorStatus == 0)
		{
			DialogBox(NULL, MAKEINTRESOURCE(IDD_ALLOW_REMOTE), NULL, AllowRemoteDialogHandler);
		}
	}
	else if (argc > 1 && (strcasecmp(argv[1], "-tratest") == 0))
	{
#ifdef TRA_DEBUG
		ILIBLOGMESSAGEX("[ServiceMain] - tratest %s\n", argv[2]);
		printf("[ServiceMain] -tratest %s\n", argv[2]);
#endif

		// 20190416 pjh
		g_serviceMode = FALSE;
		g_testMode = TRUE;

		memset((void*)&g_testAgentName, 0x00, sizeof(g_testAgentName));
		strcpy_s(g_testAgentName, sizeof(g_testAgentName), argv[2]);

		UninstallService(serviceFileOld);
		FreeConsole();

		CreateThread(NULL, 0, &StartTempAgent, NULL, 0, NULL);

		DialogBox(NULL, MAKEINTRESOURCE(IDD_GUI_DEFAULT), NULL, GuiDialogHandler);
	}
	else
	{
		UninstallService(serviceFileOld);
		if (argc > 1 && !strcmp(argv[1], "-recovery")==0)
		{
			// See if we need to run as a script engine
			if (argc >= 2 && ILibString_EndsWith(argv[1], -1, ".js", 3) != 0)
			{
				SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE); // Set SIGNAL on windows to listen for Ctrl-C

				__try
				{
					agent = MeshAgent_Create(0);
					agent->runningAsConsole = 1;
					MeshAgent_Start(agent, argc, argv);
					MeshAgent_Destroy(agent);
					agent = NULL;
				}
				__except (ILib_WindowsExceptionFilterEx(GetExceptionCode(), GetExceptionInformation(), &winException))
				{
					ILib_WindowsExceptionDebugEx(&winException);
				}
			}
			else
			{
#ifdef _MINCORE
				printf("Mesh Agent available switches:\r\n  run               Start as a console agent.\r\n  connect           Start as a temporary console agent.\r\n  restart           Restart the service.\r\n  stop              Stop the service.\r\n  state             Display the running state of the service.\r\n  -signcheck        Perform self-check.\r\n  -install          Install the service from this location.\r\n  -uninstall        Remove the service from this location.\r\n  -nodeidhex        Return the current agent identifier.\r\n  -proxy:host:port  Specifiy an HTTPS proxy (after -fullinstall only).\r\n  -tag:xxx          Specifiy a agent tag  (after -fullinstall only).\r\n\r\n  -resetnodeid      Reset the NodeID next time the service is started.");
#else
				printf("Mesh Agent available switches:\r\n  run               Start as a console agent.\r\n  connect           Start as a temporary console agent.\r\n  start             Start the service.\r\n  restart           Restart the service.\r\n  stop              Stop the service.\r\n  state             Display the running state of the service.\r\n  -signcheck        Perform self-check.\r\n  -install          Install the service from this location.\r\n  -uninstall        Remove the service from this location.\r\n  -nodeidhex        Return the current agent identifier.\r\n  -fullinstall      Copy agent into program files, install and launch.\r\n  -fulluninstall    Stop agent and clean up the program files location.\r\n  -proxy:host:port  Specifiy an HTTPS proxy (after -fullinstall only).\r\n  -tag:xxx          Specifiy a agent tag  (after -fullinstall only).\r\n  -resetnodeid      Reset the NodeID next time the service is started.");
#endif
			}
		}
		else
		{
#ifndef _MINCORE

			RunGuiMeshAgent();

			initFileLog();
			startFileLog(LOG_FULL_DIR);
			//writeFileLog("TEST----");

			if (RunService(argc, argv) == 0 && GetLastError() == ERROR_FAILED_SERVICE_CONTROLLER_CONNECT)
			{
				FreeConsole();

				/*
				if (IsAdmin() == FALSE)
				{
					MessageBox(NULL, TEXT("Must run as administrator"), TEXT("TRA Agent"), MB_OK | MB_ICONERROR);
				}
				else
				{
					DialogBox(NULL, MAKEINTRESOURCE(IDD_INSTALLDIALOG), NULL, DialogHandler);
				}
				*/

				g_serviceMode = FALSE;

				WORD wVersionRequested = MAKEWORD(2, 0);  // WinSock 2.0 夸没
				WSADATA wsaData;
				int nErrorStatus;

				nErrorStatus = WSAStartup(wVersionRequested, &wsaData);

				// 20190424 pjh
				m_ThreadConsole = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)(DoUdpConsoleRecv), (LPVOID)NULL, 0, &threadConsoleID);
				ResumeThread(m_ThreadConsole);

				DialogBox(NULL, MAKEINTRESOURCE(IDD_INSTALLDIALOG), NULL, DialogHandler);
			}
		}
#else
		RunService(argc, argv);
#endif
	}


	if (argc > 1 && (strcasecmp(argv[1], "-gui") == 0))
	{
		DestroyCursorDetector();
		DeleteTrayIcon();
	}
	else
	{
		// pjh 20190328 Red Dot (Run mode)
		destroy_rc_wnds();
	}

	// 20191023 pjh
	if (g_me_version)
	{
		g_timer_shutdown = TRUE;
		g_timer_use = FALSE;
	}

	CoUninitialize();
	wmain_free(argv);
	return 0;
}

char* getMshSettings(char* fileName, char* selfexe, char** meshname, char** meshid, char** serverid, char** serverurl, char** installFlags)
{
	char* importFile;
	int eq, importFileLen;
	parser_result *pr;
	parser_result_field *f;

	*meshname = *meshid = *serverid = *serverurl = *installFlags = NULL;
	importFileLen = ILibReadFileFromDiskEx(&importFile, fileName);
	if (importFile == NULL) {
		// Could not find the .msh file, see if there is one inside our own executable.
		FILE *tmpFile = NULL;
		char exeMeshPolicyGuid[] = { 0xB9, 0x96, 0x01, 0x58, 0x80, 0x54, 0x4A, 0x19, 0xB7, 0xF7, 0xE9, 0xBE, 0x44, 0x91, 0x4C, 0x19 };
		char tmpHash[16];

		fopen_s(&tmpFile, selfexe, "rb");
		if (tmpFile == NULL) { return NULL; } // Could not open our own executable

		fseek(tmpFile, -16, SEEK_END);
		ignore_result(fread(tmpHash, 1, 16, tmpFile)); // Read the GUID
		if (memcmp(tmpHash, exeMeshPolicyGuid, 16) == 0) { // If this is the Mesh policy file guid, we found a MSH file
														   // Found embedded MSH File
			fseek(tmpFile, -20, SEEK_CUR);
			if (fread((void*)&importFileLen, 1, 4, tmpFile) == 4) { // Read the length of the MSH file
				importFileLen = ntohl(importFileLen);
				if ((importFileLen >= 20000) || (importFileLen < 1)) { fclose(tmpFile); return NULL; }
				fseek(tmpFile, -4 - importFileLen, SEEK_CUR);
				if ((importFile = malloc(importFileLen + 1)) == NULL) { fclose(tmpFile); return NULL; }
				if (fread(importFile, 1, importFileLen, tmpFile) != importFileLen) { fclose(tmpFile); free(importFile); return NULL; }
				importFile[importFileLen] = 0;
			}
		}
		else {
			fclose(tmpFile);
			return NULL;
		}
		fclose(tmpFile);
	}

	pr = ILibParseString(importFile, 0, importFileLen, "\n", 1);
	f = pr->FirstResult;
	while (f != NULL) {
		f->datalength = ILibTrimString(&(f->data), f->datalength);
		if (f->data[0] != 35) { // Checking to see if this line is commented out
			eq = ILibString_IndexOf(f->data, f->datalength, "=", 1);
			if (eq > 0) {
				char *key, *val;
				int keyLen, valLen;

				key = f->data;
				keyLen = eq;
				key[keyLen] = 0;
				val = key + keyLen + 1;
				valLen = f->datalength - keyLen - 1;
				if (val[valLen - 1] == 13) { --valLen; }
				valLen = ILibTrimString(&val, valLen);
				val[valLen] = 0;

				if (keyLen == 8 && memcmp("MeshName", key, keyLen) == 0) { *meshname = val; }
				if (keyLen == 6 && memcmp("MeshID", key, keyLen) == 0) { *meshid = val; }
				if (keyLen == 8 && memcmp("ServerID", key, keyLen) == 0) { *serverid = val; }
				if (keyLen == 10 && memcmp("MeshServer", key, keyLen) == 0) { *serverurl = val; }
				if (keyLen == 12 && memcmp("InstallFlags", key, keyLen) == 0) { *installFlags = val; }
			}
		}
		f = f->NextResult;
	}
	ILibDestructParserResults(pr);
	return importFile;
}


#ifndef _MINCORE

// Start as a temporary mesh agent.
DWORD WINAPI StartTempAgent(_In_ LPVOID lpParameter)
{
	ILib_DumpEnabledContext winException;
	char selfexe[_MAX_PATH];
	char *selfexe_ptr[] = { selfexe };
	WCHAR str[_MAX_PATH];
	size_t len;
	char *integratedJavaScript;
	int integragedJavaScriptLen;
	char setup1[_MAX_PATH];
	int setup1len;

#ifdef TRA_DEBUG
	printf("[ServiceMain] StartTempAgent (begin). \n");
	ILIBLOGMESSAGEX("[ServiceMain] StartTempAgent (begin). \n");
#endif

	ILibDuktape_ScriptContainer_CheckEmbedded(&integratedJavaScript, &integragedJavaScriptLen);

	CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

	// Get our own executable name
	if (GetModuleFileNameW(NULL, str, _MAX_PATH) > 5) { wcstombs_s(&len, selfexe, _MAX_PATH, str, _MAX_PATH); }

	// Setup proxy filenames
	if ((setup1len = (int)strnlen_s(selfexe, sizeof(selfexe))) >= 4) {
		memcpy_s(setup1, sizeof(setup1), selfexe, setup1len);
		memcpy_s(setup1 + (setup1len - 3), sizeof(setup1) - setup1len - 3, "proxy", 6);

		// Try to setup the proxy file
		WINHTTP_CURRENT_USER_IE_PROXY_CONFIG proxyEx;
		if (WinHttpGetIEProxyConfigForCurrentUser(&proxyEx))
		{
			if (proxyEx.lpszProxy != NULL)
			{
				FILE *SourceFile = NULL;
				size_t len;
				if (wcstombs_s(&len, ILibScratchPad, 4095, proxyEx.lpszProxy, 2000) == 0)
				{
					char* ptr = strstr(ILibScratchPad, "https=");
					if (ptr != NULL)
					{
						char* ptr2 = strstr(ptr, ";");
						ptr += 6;
						if (ptr2 != NULL) ptr2[0] = 0;
					}
					else
					{
						ptr = ILibScratchPad;
					}
					fopen_s(&SourceFile, setup1, "wb");
					if (SourceFile != NULL)
					{
						if (fwrite(ptr, sizeof(char), strnlen_s(ptr, sizeof(ILibScratchPad)), SourceFile)) {}
						fclose(SourceFile);
					}
				}
				GlobalFree(proxyEx.lpszProxy);
			}

			// Release the rest of the proxy settings
			if (proxyEx.lpszAutoConfigUrl != NULL) GlobalFree(proxyEx.lpszAutoConfigUrl);
			if (proxyEx.lpszProxyBypass != NULL) GlobalFree(proxyEx.lpszProxyBypass);
		}
	}

	// Launch the temporary agent
	__try
	{
#ifdef TRA_DEBUG
		printf("[ServiceMain] StartTempAgent (begin). \n");
		ILIBLOGMESSAGEX("[ServiceMain] StartTempAgent (real). \n");
#endif

		agent = MeshAgent_Create(MeshCommand_AuthInfo_CapabilitiesMask_TEMPORARY);
		agent->meshCoreCtx_embeddedScript = integratedJavaScript;
		agent->meshCoreCtx_embeddedScriptLen = integragedJavaScriptLen;
		agent->runningAsConsole = 1;
		MeshAgent_Start(agent, 1, selfexe_ptr);
		//retCode = agent->exitCode;
		MeshAgent_Destroy(agent);
		agent = NULL;
	}
	__except (ILib_WindowsExceptionFilterEx(GetExceptionCode(), GetExceptionInformation(), &winException))
	{
#ifdef TRA_DEBUG
		printf("[ServiceMain] StartTempAgent exception. \n");
		ILIBLOGMESSAGEX("[ServiceMain] StartTempAgent exception. \n");
#endif
		ILib_WindowsExceptionDebugEx(&winException);
	}

	CoUninitialize();
	return(0);
}

// Message handler for dialog box.
INT_PTR CALLBACK DialogHandler(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	char *fileName = NULL, *meshname = NULL, *meshid = NULL, *serverid = NULL, *serverurl = NULL, *installFlags = NULL, *mshfile = NULL;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		{
			// Get the current service running state
			int r = GetServiceState(serviceFile);
			char* txt = "";
			char selfexe[_MAX_PATH];

			switch (r)
			{
			case 0:
				txt = "Error";
				break;
			case SERVICE_STOPPED: 
				txt = "Stopped";
				break;
			case SERVICE_START_PENDING: 
				txt = "Start Pending";
				break;
			case SERVICE_STOP_PENDING: 
				txt = "Stop Pending";
				break;
			case SERVICE_RUNNING: 
				txt = "Running";

				// pjh 20190427
				EnableWindow(GetDlgItem(hDlg, IDC_CONNECTBUTTON), FALSE);
				break;
			case SERVICE_CONTINUE_PENDING: 
				txt = "Continue Pending";
				break;
			case SERVICE_PAUSE_PENDING: 
				txt = "Pause Pending";
				break;
			case SERVICE_PAUSED: 
				txt = "Paused";
				break;
			case 100: 
				txt = "Not Installed";
				break;
			}
			SetWindowTextA( GetDlgItem( hDlg, IDC_STATUSTEXT ), txt);

			// Get current executable path
			GetModuleFileNameA(NULL, selfexe, MAX_PATH);
			fileName = MeshAgent_MakeAbsolutePath(selfexe, ".msh");

			{
				DWORD               dwSize = 0;
				BYTE                *pVersionInfo = NULL;
				VS_FIXEDFILEINFO    *pFileInfo = NULL;
				UINT                pLenFileInfo = 0;
				int major, minor, hotfix, other;

				if ((dwSize = GetFileVersionInfoSize(selfexe, NULL)))
				{					
					if ((pVersionInfo = malloc(dwSize)) == NULL) { ILIBCRITICALEXIT(254); }
					if (GetFileVersionInfo(selfexe, 0, dwSize, pVersionInfo))
					{
						if (VerQueryValue(pVersionInfo, TEXT("\\"), (LPVOID*)&pFileInfo, &pLenFileInfo))
						{
							// Display the version of this software
							major = (pFileInfo->dwFileVersionMS >> 16) & 0xffff;
							minor = (pFileInfo->dwFileVersionMS) & 0xffff;
							hotfix = (pFileInfo->dwFileVersionLS >> 16) & 0xffff;
							other = (pFileInfo->dwFileVersionLS) & 0xffff;
#ifdef _WIN64
							sprintf_s(ILibScratchPad, sizeof(ILibScratchPad), "v%d.%d.%d, 64bit", major, minor, hotfix);
#else
							sprintf_s(ILibScratchPad, sizeof(ILibScratchPad), "v%d.%d.%d", major, minor, hotfix);
#endif
							SetWindowTextA(GetDlgItem(hDlg, IDC_VERSIONTEXT), ILibScratchPad);
						}
					}
					free(pVersionInfo);
				}
			}

			if ((mshfile = getMshSettings(fileName, selfexe, &meshname, &meshid, &serverid, &serverurl, &installFlags)) != NULL)
			{
				// Set text in the dialog box
				int installFlagsInt = 0;
				WINDOWPLACEMENT lpwndpl;
				if (installFlags != NULL) { installFlagsInt = atoi(installFlags); }
				if (strnlen_s(meshid, 255) > 50) { meshid += 2; meshid[42] = 0; }
				if (strnlen_s(serverid, 255) > 50) { serverid[42] = 0; }
				SetWindowTextA(GetDlgItem(hDlg, IDC_POLICYTEXT), (meshid != NULL) ? meshname : "(None)");
				SetWindowTextA(GetDlgItem(hDlg, IDC_HASHTEXT), (meshid != NULL) ? meshid : "(None)");

				// pjh - 20190504 /////////////////////////////////////////////////////////////////
				char visibleurl[255];
				memset(visibleurl, 0x00, sizeof(visibleurl));
				strcpy_s(visibleurl, sizeof(visibleurl), serverurl);

				int i = 0;
				char *ptr = strtok(visibleurl, "/");
				while (ptr != NULL)
				{
					if (i > 0)
						break;

					i++;
					ptr = strtok(NULL, "/");
				}
				///////////////////////////////////////////////////////////////////////////////////

				SetWindowTextA(GetDlgItem(hDlg, IDC_SERVERLOCATION), (ptr != NULL) ? ptr : "(None)");
				SetWindowTextA(GetDlgItem(hDlg, IDC_SERVERID), (serverid != NULL) ? serverid : "(None)");
				free(mshfile);
				if (meshid == NULL) { EnableWindow(GetDlgItem(hDlg, IDC_CONNECTBUTTON), FALSE); }
				if ((installFlagsInt & 3) == 1) {
					// Temporary Agent Only
					ShowWindow(GetDlgItem(hDlg, IDC_INSTALLBUTTON), SW_HIDE);
					ShowWindow(GetDlgItem(hDlg, IDC_UNINSTALLBUTTON), SW_HIDE);
					GetWindowPlacement(GetDlgItem(hDlg, IDC_INSTALLBUTTON), &lpwndpl);
					SetWindowPlacement(GetDlgItem(hDlg, IDC_CONNECTBUTTON), &lpwndpl);
				}  else if ((installFlagsInt & 3) == 2) {
					// Background Only
					ShowWindow(GetDlgItem(hDlg, IDC_CONNECTBUTTON), SW_HIDE);
				} else if ((installFlagsInt & 3) == 3) {
					// Uninstall only
					GetWindowPlacement(GetDlgItem(hDlg, IDC_INSTALLBUTTON), &lpwndpl);
					SetWindowPlacement(GetDlgItem(hDlg, IDC_UNINSTALLBUTTON), &lpwndpl);
					ShowWindow(GetDlgItem(hDlg, IDC_INSTALLBUTTON), SW_HIDE);
					ShowWindow(GetDlgItem(hDlg, IDC_CONNECTBUTTON), SW_HIDE);
				}
			}
			else
			{
				EnableWindow(GetDlgItem(hDlg, IDC_CONNECTBUTTON), FALSE);
			}

			// pjh 20190504
			HICON hIcon;

			hIcon = LoadImage(g_hres,
				MAKEINTRESOURCE(IDI_ICON1),
				IMAGE_ICON,
				GetSystemMetrics(SM_CXSMICON),
				GetSystemMetrics(SM_CYSMICON),
				0);
			if (hIcon)
			{
				SendMessage(hDlg, WM_SETICON, ICON_SMALL, (LPARAM)hIcon);
			}


			// pjh 20190424
			UDPMSG udpmsg;

			UdpInit();
			UdpConnect(UDPPROC_SERVICE_PORT);

			memset((void*)&udpmsg, 0x00, sizeof(udpmsg));
			udpmsg.command = UDP_REQUEST_REMOTE_LIST;
			UdpSend(udpmsg);

			// 20190424
			hwndList = GetDlgItem(hDlg, IDC_SESSION_LIST);

			// 20190512
			UdpInitGui();
			UdpConnectGui(UDPPROC_CONSOLE_PORT);

			return (INT_PTR)TRUE;
		}
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));

#ifdef _DEBUG
			_CrtCheckMemory();
			_CrtDumpMemoryLeaks();
#endif

			return (INT_PTR)TRUE;
		}
		else if (LOWORD(wParam) == IDC_INSTALLBUTTON || LOWORD(wParam) == IDC_UNINSTALLBUTTON)
		{
			UDPMSG udpmsg;
			UdpInitGui();
			UdpConnectGui(UDPPROC_GUI_PORT);
			memset((void*)&udpmsg, 0x00, sizeof(udpmsg));
			udpmsg.command = UDP_GUI_TERMINATE;
			UdpSendGui(udpmsg);

			BOOL result = FALSE;

			EnableWindow( GetDlgItem( hDlg, IDC_INSTALLBUTTON ), FALSE );
			EnableWindow( GetDlgItem( hDlg, IDC_UNINSTALLBUTTON ), FALSE );
			EnableWindow( GetDlgItem( hDlg, IDCANCEL ), FALSE );

			if (IsAdmin() == TRUE)
			{
				// We are already administrator, just install/uninstall now.
				if (LOWORD(wParam) == IDC_INSTALLBUTTON) { fullinstall(0, NULL, 0, NULL, 0); } else { fullinstall(1, NULL, 0, NULL, 0); }
				result = TRUE;
			}
			else
			{
				// We need to request admin escalation
				if (LOWORD(wParam) == IDC_INSTALLBUTTON) { result = RunAsAdmin("-fullinstall"); } else { result = RunAsAdmin("-fulluninstall"); }
			}

			if (result)
			{
				EndDialog(hDlg, LOWORD(wParam));
			}
			else
			{
				EnableWindow(GetDlgItem(hDlg, IDC_INSTALLBUTTON), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDC_UNINSTALLBUTTON), TRUE);
				EnableWindow(GetDlgItem(hDlg, IDCANCEL), TRUE);
			}

#ifdef _DEBUG
			_CrtCheckMemory();
			_CrtDumpMemoryLeaks();
#endif

			return (INT_PTR)TRUE;
		}
		else if (LOWORD(wParam) == IDC_CONNECTBUTTON) {
			EnableWindow(GetDlgItem(hDlg, IDC_INSTALLBUTTON), FALSE);
			EnableWindow(GetDlgItem(hDlg, IDC_UNINSTALLBUTTON), FALSE);
			EnableWindow(GetDlgItem(hDlg, IDC_CONNECTBUTTON), FALSE);
			SetWindowTextA(GetDlgItem(hDlg, IDC_STATUSTEXT), "Running as temporary agent");

			// 20190512 pjh
			EnableWindow(GetDlgItem(hDlg, IDDISCONNECT), FALSE);

			CreateThread(NULL, 0, &StartTempAgent, NULL, 0, NULL);
			return (INT_PTR)TRUE;
		}
		// 20190424
		else if (LOWORD(wParam) == IDDISCONNECT) {

			UDPMSG udpmsg;

			int lbItem = (int)SendMessage(hwndList, LB_GETCURSEL, 0, 0);

			// Get item data.
			int i = (int)SendMessage(hwndList, LB_GETITEMDATA, lbItem, 0);

			memset((void*)&udpmsg, 0x00, sizeof(udpmsg));

			udpmsg.command = UDP_REQUEST_FORCE_DISCONNECT;
			udpmsg.socketNumber = i;

			UdpSend(udpmsg);
			//UdpSendGui(udpmsg);

			return (INT_PTR)TRUE;
		}
		else if (LOWORD(wParam) == IDC_SESSION_LIST) {

			if (HIWORD(wParam) == LBN_SELCHANGE) {

				/*
				HWND hwndList = GetDlgItem(hDlg, IDC_SESSION_LIST);
				char testStr[255];

				int lbItem = (int)SendMessage(hwndList, LB_GETCURSEL, 0, 0);

				// Get item data.
				int i = (int)SendMessage(hwndList, LB_GETITEMDATA, lbItem, 0);

				memset(testStr, 0x00, sizeof(testStr));
				sprintf(testStr, "Value : %d", i);
				MessageBox(NULL, testStr, TEXT("TRA Agent"), MB_OK | MB_ICONERROR);
				*/
			}
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

// Message handler for dialog box.
INT_PTR CALLBACK GuiDialogHandler(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	//char *fileName = NULL, *meshname = NULL, *meshid = NULL, *serverid = NULL, *serverurl = NULL, *installFlags = NULL, *mshfile = NULL;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
	{
		SetWindowPos(hDlg, NULL, 0, 0, 1, 1, SWP_HIDEWINDOW);

		long style = GetWindowLong(hDlg, GWL_EXSTYLE);
		style &= ~(WS_VISIBLE);
		style |= WS_EX_TOOLWINDOW;
		style &= ~(WS_EX_APPWINDOW);

		ShowWindow(hDlg, SW_HIDE);
		SetWindowLong(hDlg, GWL_EXSTYLE, style);
		ShowWindow(hDlg, SW_SHOW);

		UdpInit();
		UdpConnect(UDPPROC_SERVICE_PORT);

		Sleep(500);

		UDPMSG udpmsg;
		memset((void*)&udpmsg, 0x00, sizeof(udpmsg));
		udpmsg.command = UDP_GUI_REQUEST_REDDOT;
		UdpSend(udpmsg);

		return (INT_PTR)TRUE;
	}
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			//EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK AllowRemoteDialogHandler(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	//char *fileName = NULL, *meshname = NULL, *meshid = NULL, *serverid = NULL, *serverurl = NULL, *installFlags = NULL, *mshfile = NULL;

	UDPMSG udpmsg;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
	{
		SetWindowPos(hDlg, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
		SetWindowPos(hDlg, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

		UdpInit();
		UdpConnect(UDPPROC_SERVICE_PORT);

		return (INT_PTR)FALSE;
}
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK)
		{
			memset((void*)&udpmsg, 0x00, sizeof(udpmsg));

			udpmsg.command = UDP_ALLOW_REMOTE;
			udpmsg.socketNumber = g_socketNumber;

			UdpSend(udpmsg);

			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		else if (LOWORD(wParam) == IDCANCEL)
		{
			memset((void*)&udpmsg, 0x00, sizeof(udpmsg));

			udpmsg.command = UDP_DISALLOW_REMOTE;
			udpmsg.socketNumber = 0;

			UdpSend(udpmsg);

			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

// pjh 20190424
LRESULT CALLBACK DoUdpConsoleRecv(LPVOID lpParameter)
{
	int m_udpSocket;
	struct sockaddr_in m_udpServ, m_udpClient;
	socklen_t m_udpSockLen;
	int m_enable = 1;
	//UDPMSG udpmsg;

	char listText[255];

	m_udpSocket = socket(AF_INET, SOCK_DGRAM, 0);

	m_udpServ.sin_family = AF_INET;
	m_udpServ.sin_port = htons(UDPPROC_CONSOLE_PORT);
	m_udpServ.sin_addr.s_addr = htonl(INADDR_ANY);
	m_udpSockLen = sizeof(m_udpServ);

	setsockopt(m_udpSocket, SOL_SOCKET, SO_REUSEADDR, (const char*)&m_enable, sizeof(m_enable));

	memset((char*)&m_udpClient, 0x00, m_udpSockLen);

	bind(m_udpSocket, (struct sockaddr*)&m_udpServ, m_udpSockLen);

#ifdef TRA_DEBUG
	ILIBLOGMESSSAGE("DoUdpConsoleRecv start...\n");
#endif

	while (1)
	{
		int nByte = 0;
		UDPMSG udpmsg;
		memset((void*)&udpmsg, 0x00, sizeof(udpmsg));

#ifdef TRA_DEBUG
		ILIBLOGMESSSAGE("DoUdpConsoleRecv recvfrom() start.\n");
#endif

		nByte = recvfrom(m_udpSocket, (char*)&udpmsg, sizeof(udpmsg), 0, (struct sockaddr*)&m_udpClient, &m_udpSockLen);

#ifdef TRA_DEBUG
		ILIBLOGMESSAGEX("DoUdpConsoleRecv recvfrom() - %d\n", nByte);
#endif

		if (nByte > 0)
		{
#ifdef TRA_DEBUG
			ILIBLOGMESSAGEX("DoUdpConsoleRecv recvfrom() data : [%d][%d]", udpmsg.command, udpmsg.socketNumber);
#endif

			switch (udpmsg.command)
			{
			case UDP_REQUEST_REMOTE_LIST_ACK:

				if (udpmsg.type == TRA_CLIENT_COMMAND_ADD)
				{
					//MessageBox(NULL, TEXT("TRA_CLIENT_COMMAND_ADD"), TEXT("TRA Agent"), MB_OK | MB_ICONERROR);
					Sleep(300);

					if (strlen(udpmsg.com_name) == 0 || strlen(udpmsg.ip_addr) == 0)
					{
						break;
					}

					int i;
					int count = SendMessage(hwndList, LB_GETCOUNT, 0, 0);

					for (i = 0; i < count; i++) {

						int itemData = (int)SendMessage(hwndList, LB_GETITEMDATA, i, 0);

						if (itemData == udpmsg.socketNumber) {
							SendMessage(hwndList, LB_DELETESTRING, i, 0);
						}
					}

					memset(listText, 0x00, sizeof(listText));
					sprintf(listText, "%s (%s) - [%d]", udpmsg.com_name, udpmsg.ip_addr, udpmsg.socketNumber);

					int pos = (int)SendMessage(hwndList, LB_ADDSTRING, 0,
						(LPARAM)listText);
					SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM)udpmsg.socketNumber);
				}
				else if (udpmsg.type == TRA_CLIENT_COMMAND_DEL)
				{
					//MessageBox(NULL, TEXT("TRA_CLIENT_COMMAND_DEL"), TEXT("TRA Agent"), MB_OK | MB_ICONERROR);
					Sleep(300);

					int i;
					int count = SendMessage(hwndList, LB_GETCOUNT, 0, 0);

					for (i = 0; i < count; i++) {

						int itemData = (int)SendMessage(hwndList, LB_GETITEMDATA, i, 0);

						if (itemData == udpmsg.socketNumber) {
							SendMessage(hwndList, LB_DELETESTRING, i, 0);
						}
					}
				}

				break;

			/*
			case UDP_REQUEST_FORCE_DISCONNECT:

				ILIBLOGMESSSAGE("DoUdpConsoleRecv UDP_REQUEST_FORCE_DISCONNECT...\n");
				ILibDuktape_ScriptContainer_FORCE_DISCONNECT(udpmsg.socketNumber);
				break;
			*/
			}
		}
	}

	return 0L;
}

// pjh 20190424
LRESULT CALLBACK DoUdpGuiRecv(LPVOID lpParameter)
{
	int m_udpSocket;
	struct sockaddr_in m_udpServ, m_udpClient;
	socklen_t m_udpSockLen;
	int m_enable = 1;

	char testStr[255];

	UdpInitGui();
	UdpConnectGui(UDPPROC_GUI_PORT);

	UDPMSG udpmsg;
	memset((void*)&udpmsg, 0x00, sizeof(udpmsg));
	udpmsg.command = UDP_GUI_TERMINATE;
	UdpSendGui(udpmsg);
	Sleep(300);

	m_udpSocket = socket(AF_INET, SOCK_DGRAM, 0);

	m_udpServ.sin_family = AF_INET;
	m_udpServ.sin_port = htons(UDPPROC_GUI_PORT);
	m_udpServ.sin_addr.s_addr = htonl(INADDR_ANY);
	m_udpSockLen = sizeof(m_udpServ);

	setsockopt(m_udpSocket, SOL_SOCKET, SO_REUSEADDR, (const char*)&m_enable, sizeof(m_enable));

	memset((char*)&m_udpClient, 0x00, m_udpSockLen);

	if (bind(m_udpSocket, (struct sockaddr*)&m_udpServ, m_udpSockLen) < 0) {
#ifdef TRA_DEBUG
		ILIBLOGMESSSAGE("DoUdpGuiRecv bind fail...\n");
#endif
		DeleteTrayIcon();

		Sleep(100);

		CoUninitialize();
		exit(0);
	}

#ifdef TRA_DEBUG
	ILIBLOGMESSSAGE("DoUdpGuiRecv start...\n");
#endif

	while (1)
	{
		int nByte = 0;
		UDPMSG udpmsg;
		memset((void*)&udpmsg, 0x00, sizeof(udpmsg));

#ifdef TRA_DEBUG
		ILIBLOGMESSSAGE("DoUdpGuiRecv recvfrom() start.\n");
#endif

		nByte = recvfrom(m_udpSocket, (char*)&udpmsg, sizeof(udpmsg), 0, (struct sockaddr*)&m_udpClient, &m_udpSockLen);

#ifdef TRA_DEBUG
		ILIBLOGMESSAGEX("DoUdpGuiRecv recvfrom() - %d\n", nByte);
#endif

		if (nByte > 0)
		{
#ifdef TRA_DEBUG
			ILIBLOGMESSAGEX("DoUdpGuiRecv recvfrom() data : [%d][%d]", udpmsg.command, udpmsg.socketNumber);
#endif

			switch (udpmsg.command)
			{
			case UDP_GUI_TERMINATE:

				DeleteTrayIcon();

				Sleep(100);
				CoUninitialize();
				exit(0);
				break;

			case UDP_GUI_REDDOT_ON:

				show_rc_wnds(2);
				break;

			case UDP_GUI_REDDOT_OFF:

				hide_rc_wnds();
				break;

			case UDP_TEMP:

				memset(testStr, 0x00, sizeof(testStr));
				sprintf(testStr, "Value : %d", udpmsg.type);

				MessageBox(NULL, testStr, TEXT("TRA Agent"), MB_OK);
				break;
				
				// For Test
				/*
			case UDP_GUI_CURSOR_CHANGED:

				memset(testStr, 0x00, sizeof(testStr));
				sprintf(testStr, "Value : %d", udpmsg.type);

				MessageBox(NULL, testStr, TEXT("TRA Agent"), MB_OK);
				break;
				*/
			}
		}
		else
		{
			DeleteTrayIcon();

			Sleep(100);

			CoUninitialize();
			exit(0);
		}
	}

	return 0L;
}

LRESULT CALLBACK DoThreadSession(LPVOID lpParameter)
{
	Sleep(25000);

	RunGuiMeshAgent();
	return 0L;
}

LRESULT CALLBACK DoKVMRecv(LPVOID lpParameter)
{
	int m_udpSocket;
	struct sockaddr_in m_udpServ, m_udpClient;
	socklen_t m_udpSockLen;
	int m_enable = 1;

	//char testStr[255];

	m_udpSocket = socket(AF_INET, SOCK_DGRAM, 0);

	m_udpServ.sin_family = AF_INET;

	if(g_kvm_type == 1)
		m_udpServ.sin_port = htons(UDPPROC_KVM1_PORT);
	else
		m_udpServ.sin_port = htons(UDPPROC_KVM0_PORT);
	
	m_udpServ.sin_addr.s_addr = htonl(INADDR_ANY);
	m_udpSockLen = sizeof(m_udpServ);

	setsockopt(m_udpSocket, SOL_SOCKET, SO_REUSEADDR, (const char*)&m_enable, sizeof(m_enable));

	memset((char*)&m_udpClient, 0x00, m_udpSockLen);

	bind(m_udpSocket, (struct sockaddr*)&m_udpServ, m_udpSockLen);

#ifdef TRA_DEBUG
	ILIBLOGMESSSAGE("DoKVMRecv start...\n");
#endif

	while (1)
	{
		int nByte = 0;
		UDPMSG udpmsg;
		memset((void*)&udpmsg, 0x00, sizeof(udpmsg));

#ifdef TRA_DEBUG
		ILIBLOGMESSSAGE("DoKVMRecv recvfrom() start.\n");
#endif

		nByte = recvfrom(m_udpSocket, (char*)&udpmsg, sizeof(udpmsg), 0, (struct sockaddr*)&m_udpClient, &m_udpSockLen);

#ifdef TRA_DEBUG
		ILIBLOGMESSAGEX("DoKVMRecv recvfrom() - %d\n", nByte);
#endif

		if (nByte > 0)
		{
#ifdef TRA_DEBUG
			ILIBLOGMESSAGEX("DoUdpConsoleRecv recvfrom() data : [%d][%d]", udpmsg.command, udpmsg.socketNumber);
#endif

			switch (udpmsg.command)
			{
				/*
			case UDP_KVM_TERMINATE:

				kvm_cleanup();
				return;
				*/
				/*
			case UDP_KVM_CURSOR_CHANGED:

				memset(testStr, 0, sizeof(testStr));
				sprintf(testStr, "CURSOR_%d", udpmsg.type);

				if (g_DataChannel != NULL)
				{
					ILibWrapper_WebRTC_DataChannel_SendString(g_DataChannel, testStr, strlen(testStr));
				}
				else 
				{
					UDPMSG udpmsg;
					UdpInitGui();
					UdpConnectGui(UDPPROC_GUI_PORT);
					memset((void*)&udpmsg, 0x00, sizeof(udpmsg));
					udpmsg.command = UDP_TEMP;
					UdpSendGui(udpmsg);
				}
				break;
				*/
			}
		}
	}

	return 0L;
}

#endif

#ifdef _MINCORE
BOOL WINAPI AreFileApisANSI(void) { return FALSE; }
VOID WINAPI FatalAppExitA(_In_ UINT uAction, _In_ LPCSTR lpMessageText) {}
HANDLE WINAPI CreateSemaphoreW(_In_opt_  LPSECURITY_ATTRIBUTES lpSemaphoreAttributes, _In_ LONG lInitialCount, _In_ LONG lMaximumCount, _In_opt_ LPCWSTR lpName)
{
	return 0;
}
#endif
